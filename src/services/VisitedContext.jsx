import React, { useState, createContext } from "react";

export const VisitedCollectionsContext = createContext();

export const VisitedCollectionsContextProvider = ({ children }) => {
  const [isCollectionsVisited, setIsCollectionsVisited] = useState(false);

  return (
    <VisitedCollectionsContext.Provider
      value={{
        isCollectionsVisited,
        setIsCollectionsVisited,
      }}
    >
      {children}
    </VisitedCollectionsContext.Provider>
  );
};
