import React, { useState, createContext } from "react";

export const IntroContext = createContext();

export const IntroContextProvider = ({ children }) => {
  const [visited, setVisited] = useState(false);

  return (
    <IntroContext.Provider
      value={{
        visited,
        setVisited,
      }}
    >
      {children}
    </IntroContext.Provider>
  );
};
