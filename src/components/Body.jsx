import { Typography, Box, useTheme } from "@mui/material";

const Body = ({ title, subtitle, other }) => {
  const theme = useTheme();
  return (
    <Box>
      <Typography
        variant="h3"
        fontWeight="bold"
        color={theme.palette.fonts.bodyTitle}
      >
        {title}
      </Typography>
      <Typography variant="h6" color={theme.palette.fonts.bodySubtitle}>
        {subtitle}
      </Typography>
      <Typography variant="h6" color={theme.palette.fonts.bodyOther}>
        {other}
      </Typography>
    </Box>
  );
};

export default Body;
