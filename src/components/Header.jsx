import { Typography, Box, useTheme } from "@mui/material";

const Header = ({ title, subtitle, other }) => {
  const theme = useTheme();
  return (
    <Box mb="30px">
      <Typography
        variant="h2"
        fontWeight="bold"
        sx={{
          m: "0 0 5px 0",
          color: theme.palette.fonts.headerTitle,
        }}
      >
        {title}
      </Typography>
      <Typography
        variant="h5"
        sx={{
          m: "0 0 5px 0",
          color: theme.palette.fonts.headerSubtitle,
        }}
      >
        {subtitle}
      </Typography>
      <Typography
        variant="h5"
        sx={{
          m: "0 0 5px 0",
          color: theme.palette.fonts.headerOther,
        }}
      >
        {other}
      </Typography>
    </Box>
  );
};

export default Header;
