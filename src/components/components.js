import { styled } from "@mui/system";
import { Box, Button, CircularProgress } from "@mui/material";
import { makeStyles, createStyles } from "@mui/styles";
import AceEditor from "react-ace";
import TextField from "@mui/material/TextField";
import { useMediaQuery } from "react-responsive";
import Modal from "@mui/material/Modal";
import Popover from "@mui/material/Popover";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Typography from "@mui/material/Typography";
import { keyframes } from "@emotion/react";

import "ace-builds/src-noconflict/mode-plain_text";
import "ace-builds/src-noconflict/theme-gob";
import "ace-builds/src-noconflict/theme-github";
import "../scenes/global/ace.css";

export const pulsingAnimation = (scaleValue) => keyframes`
  0% {
    transform: scale(1);
  }
  50% {
    transform: scale(${scaleValue});
  }
  100% {
    transform: scale(1);
  }
`;

const useStyles = makeStyles((theme) =>
  createStyles({
    customButton: {
      margin: "5px",
      borderRadius: 0,
      fontFamily: "Courier New",
      letterSpacing: "1px",
      width: "auto",
      whiteSpace: "nowrap",
      overflow: "hidden",
      textOverflow: "ellipsis",
      "&.Mui-disabled": {
        backgroundColor: theme.palette.buttons.generateButtonLoadingBackground,
        color: theme.palette.buttons.generateButtonLoadingText,
      },
    },
    buttonWrapper: {
      display: "flex",
      alignItems: "center",
    },
    spinnerWrapper: {
      marginLeft: "5px",
    },
  })
);

export const ConfirmationDialog = ({
  theme,
  open,
  onClose,
  title,
  content,
  onClickNo,
  onClickYes,
}) => {
  return (
    <Dialog
      open={open}
      onClose={onClose}
      PaperProps={{
        sx: {
          bgcolor: theme.palette.modal.background,
        },
      }}
    >
      <DialogTitle style={{ fontFamily: "Courier New", letterSpacing: "1px" }}>
        {title}
      </DialogTitle>
      <DialogContent>
        <Typography style={{ fontFamily: "Courier New", letterSpacing: "1px" }}>
          {content}
        </Typography>
      </DialogContent>

      <DialogActions>
        <Button
          onClick={onClickNo}
          color="primary"
          sx={{
            color: theme.palette.modal.cancelButtonText,
          }}
        >
          Cancel
        </Button>
        <Button
          // onClick={() => handleConfirmClear(selectedItem?.item || selectedItem)}
          onClick={onClickYes}
          variant="contained"
          color="primary"
          sx={{
            color: theme.palette.modal.removeButtonText,
          }}
        >
          Remove
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export const InputTextfield = ({
  id,
  focused,
  label,
  rows,
  onChange,
  value,
  onFocus,
  onBlur,
  theme,
  onKeyPress,
}) => {
  const isSmall = useMediaQuery({ maxWidth: 610 });
  const shouldHideLabel = value.length > 0;
  return (
    <TextField
      id={id}
      variant="outlined"
      label={focused || shouldHideLabel ? "" : label}
      size="medium"
      multiline
      rows={rows}
      inputProps={{ style: { fontSize: 13 } }}
      InputLabelProps={{
        style: { color: theme.palette.inputForm.text },
        shrink: focused,
      }}
      style={{
        width: isSmall ? "25rem" : "30rem",
        backgroundColor: theme.palette.inputForm.background,
        marginBottom: "10px",
      }}
      onChange={onChange}
      value={value}
      onFocus={onFocus}
      onBlur={onBlur}
      onKeyPress={onKeyPress}
      sx={{
        "& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
          borderColor: theme.palette.inputForm.border,
          borderWidth: "1px",
        },
        "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline":
          {
            borderColor: theme.palette.inputForm.border,
          },
        "& .MuiInputLabel-outlined.MuiInputLabel-shrink": {
          color: theme.palette.inputForm.text,
        },
      }}
    />
  );
};

///////////////////////////////////////////////////
//-------------------BUTTONS---------------------//
///////////////////////////////////////////////////

export const GenerateButton = ({ onClick, disabled, buttonText, loading }) => {
  const classes = useStyles();
  return (
    <Button
      variant="contained"
      color="info"
      onClick={onClick}
      disabled={disabled}
      className={classes.customButton}
    >
      <div className={classes.buttonWrapper}>
        {buttonText}
        {loading && (
          <div className={classes.spinnerWrapper}>
            <CircularProgress size={20} color="inherit" />
          </div>
        )}
      </div>
    </Button>
  );
};

export const SimpleButton = ({ onClick, text, width, theme, margin }) => {
  return (
    <Button
      variant="contained"
      onClick={onClick}
      style={{
        margin: margin || "5px 5px 5px 5px",
        borderRadius: 0,
        fontFamily: "Courier New",
        letterSpacing: "1px",
        width: width || "auto",
        whiteSpace: "nowrap",
        overflow: "hidden",
        textOverflow: "ellipsis",
        backgroundColor: theme.palette.buttons.simpleButtonBackground,
        color: theme.palette.buttons.simpleButtonText,
      }}
    >
      {text}
    </Button>
  );
};

///////////////////////////////////////////////////
//-------------------ACE-------------------------//
///////////////////////////////////////////////////

export const AceWindow = ({ value, theme }) => {
  return (
    <AceEditor
      mode="plain_text"
      theme={theme === "dark" ? "gob" : "github"}
      value={value}
      editorProps={{ $blockScrolling: Infinity }}
      readOnly={true}
      style={{ width: "100%", height: "87%" }}
    />
  );
};
export const AceComponent = ({ value, theme }) => {
  const isSmall = useMediaQuery({ maxWidth: 610 });
  return (
    <AceEditor
      mode="plain_text"
      theme={theme.palette.mode === "dark" ? "gob" : "github"}
      value={value}
      editorProps={{ $blockScrolling: Infinity }}
      style={{
        right: !isSmall ? "30px" : undefined,
        width: isSmall ? "25rem" : "30rem",
      }}
      className={
        theme.palette.mode === "dark" ? "ace-dark-theme" : "ace-light-theme"
      }
    />
  );
};

///////////////////////////////////////////////////
//-------------------MODAL-----------------------//
///////////////////////////////////////////////////

export const NotificationModalBox = styled(Box)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 400px;
  background-color: ${(props) => props.theme.palette.modal.background};
  border: 2px solid #000;
  box-shadow: 24px;
  padding: 16px;
`;
export const ConfirmationModal = ({ open, onClose, theme, generated }) => {
  return (
    <Modal
      keepMounted
      open={open}
      onClose={onClose}
      aria-labelledby="keep-mounted-modal-title"
      aria-describedby="keep-mounted-modal-description"
    >
      <NotificationModalBox theme={theme}>
        {generated.length !== 0 && (
          <>
            <Typography
              id="keep-mounted-modal-title"
              variant="h6"
              component="h2"
            >
              Playbook is created!
            </Typography>
            <Typography id="keep-mounted-modal-description" sx={{ mt: 2 }}>
              View it by pressing button on top of the editor screen
            </Typography>
          </>
        )}
      </NotificationModalBox>
    </Modal>
  );
};

///////////////////////////////////////////////////
//-------------------POPOVER---------------------//
///////////////////////////////////////////////////

export const PopoverTypography = ({
  open,
  handlePopoverOpen,
  handlePopoverClose,
  icon,
  theme,
}) => {
  return (
    <Typography
      aria-owns={open ? "mouse-over-popover" : undefined}
      aria-haspopup="true"
      onMouseEnter={handlePopoverOpen}
      onMouseLeave={handlePopoverClose}
      paddingLeft="0.5rem"
      color={theme.palette.primary.infoIcon}
      sx={{
        marginLeft: "5px",
      }}
    >
      {icon}
    </Typography>
  );
};
export const PopoverComponent = ({
  open,
  anchorEl,
  handlePopoverClose,
  text,
}) => {
  return (
    <Popover
      id="mouse-over-popover"
      sx={{
        pointerEvents: "none",
      }}
      open={open}
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "left",
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "left",
      }}
      onClose={handlePopoverClose}
      disableRestoreFocus
    >
      {text}
    </Popover>
  );
};

///////////////////////////////////////////////////
//-------------------BOXES-----------------------//
///////////////////////////////////////////////////

export const FloatingAce = styled(Box)`
  position: absolute;
  top: 80px;
  left: 200px;
  right: 50px;
  bottom: 100px;
  z-index: 9999;
  display: flex;
  align-items: center;
  justify-content: center;
`;
export const MainEnvelope = styled(Box)`
  padding: 20px;
  display: flex;
`;
export const HeadingBox = styled(Box)`
  flex: 0 0 auto;
  height: fit-content;
`;
export const HeaderAndPopoverBox = styled(Box)`
  display: flex;
  flex-direction: row;
`;
export const ImportedComponentBox = styled(Box)`
  flex: 1 1 auto;
  height: auto;
`;
export const MainBox = styled(Box)`
  padding: 20px;
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  overflow-x: hidden;
`;
export const MainBoxNoFlex = styled(Box)`
  padding: 20px;
`;
export const PacmanBox = styled(Box)`
  display: flex;
  width: 100%;
  height: 100%;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
export const NoConnectionBox = styled(Box)`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
`;

export const MainHeaderBox = styled(Box)`
  display: flex;
  justify-content: space-between;
  padding: 16px;
`;
export const LeftHeaderBox = styled(Box)``;
export const RightHeaderBox = styled(Box)``;
export const ButtonBox = styled(Box)`
  display: flex;
  justify-content: center;
`;
export const GraphButtonBox = styled(Box)`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;
