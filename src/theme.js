import { createContext, useState, useMemo } from "react";
import { createTheme } from "@mui/material/styles";

export const themeSettings = (mode) => {
  const fontFamily = "Source Sans Pro, sans-serif";

  return {
    palette: {
      mode: mode,
      ...(mode === "dark"
        ? {
            primary: {
              main: "#141b2d",
              another: "#1F2A40",
              animation: "#36d7b7",
              typewriterCaret: "white",
              infoIcon: "#b7ebde",
              errorMessageBackground: "#ffff00",
              errorMessageText: "red",
              warningMessageBackground: "blue",
              warningMessageText: "black",
              infoMessageBackground: "orange",
              infoMessageText: "purple",
            },
            buttons: {
              generateButtonLoadingBackground: "red",
              generateButtonLoadingText: "white",
              simpleButtonBackground: "yellow",
              simpleButtonText: "red",
            },
            secondary: {
              main: "#4cceac",
            },
            neutral: {
              dark: "#3d3d3d",
              main: "#666666",
              light: "#e0e0e0",
            },
            inputForm: {
              border: "#70d8bd",
            },
            background: {
              default: "#141b2d",
            },
            sidebar: {
              logo: "#e0e0e0",
              collapsedIcon: "#e0e0e0",
              disabled: "#666666",
              //isHovered: "#c2c2c2",
              menuEntry: "#e0e0e0",
              announcement: "#64D2A1",
              background: "#1F2A40",
              hover: "#868dfb !important",
              active: "#6870fa !important",
              modeSwitchIcon: "yellow",
              menuSplit: "#a3a3a3",
            },
            modal: {
              background: "#383838",
              cancelButtonText: "#ffffff",
              removeButtonText: "#ffffff",
            },
            builderNodes: {
              mainCircle: "#4BE7AB",
              circle: "#4BD5E7",
              block: "#4BD5E7",
              role: "#D5E74B",
              nodeName: "#1ECBE1",
              buttonColorFill: "#f2f218",
              buttonColorStroke: "#f2f218",
              leftButtonPressedFill: "red",
              leftButtonPressedStroke: "red",
            },
            list: {
              arrowBack: "#01F901",
              arrowForward: "#01F901",
              rowRenderer: "#e0e0e0",
            },
            accordion: {
              background: "#1E1E1E",
              typography: "#4cceac",
            },
            fonts: {
              bodyTitle: "#e0e0e0",
              bodySubtitle: "#70d8bd",
              bodyOther: "#70d8bd",
              headerTitle: "#e0e0e0",
              headerSubtitle: "#70d8bd",
              headerOther: "#70d8bd",
            },
          }
        : {
            primary: {
              main: "#F7E8C5",
              animation: "#D55E00",
              typewriterCaret: "red",
              infoIcon: "brown",
              errorMessageBackground: "red",
              errorMessageText: "white",
              warningMessageBackground: "black",
              warningMessageText: "yellow",
              infoMessageBackground: "orange",
              infoMessageText: "purple",
            },
            buttons: {
              generateButtonLoadingBackground: "blue",
              generateButtonLoadingText: "yellow",
              simpleButtonBackground: "brown",
              simpleButtonText: "black",
            },
            secondary: {
              main: "#43C0F6",
            },
            neutral: {
              dark: "#FF94B4",
              main: "#FF94B4",
              light: "#FF94B4",
            },
            inputForm: {
              border: "#70d8bd",
              text: "#9D4F07",
              background: "#E2E6F2",
            },
            background: {
              default: "#F7E8C5",
            },
            sidebar: {
              logo: "#853107",
              collapsedIcon: "#e0e0e0",
              disabled: "#666666",
              //isHovered: "#c2c2c2",
              menuEntry: "#380f1c",
              announcement: "#D55E00",
              background: "#68badd",
              hover: "#9D4F07 !important",
              active: "#1f10e8 !important",
              modeSwitchText: "brown",
              modeSwitchIcon: "black",
              menuSplit: "brown",
            },
            modal: {
              background: "#EAC680",
              cancelButtonText: "brown",
              removeButtonText: "red",
            },
            builderNodes: {
              mainCircle: "#FF7609",
              circle: "#D55E00",
              block: "#D55E00",
              role: "#D5000D",
              nodeName: "black",
              buttonColorFill: "brown",
              buttonColorStroke: "brown",
              leftButtonPressedFill: "red",
              leftButtonPressedStroke: "red",
            },
            list: {
              arrowBack: "#F81B84",
              arrowForward: "#F81B84",
              rowRenderer: "#9D4F07",
            },
            accordion: {
              background: "#E2E6F2",
              typography: "#9D4F07",
            },
            fonts: {
              bodyTitle: "#e0e0e0",
              bodySubtitle: "#70d8bd",
              bodyOther: "#D55E00",
              headerTitle: "#D55E00",
              headerSubtitle: "#D55E00",
              headerOther: "#9D4F07",
            },
          }),
    },
    typography: {
      fontFamily,
      fontSize: 12,
      h1: {
        fontFamily,
        fontSize: 40,
      },
      h2: {
        fontFamily,
        fontSize: 32,
      },
      h3: {
        fontFamily,
        fontSize: 24,
      },
      h4: {
        fontFamily,
        fontSize: 20,
      },
      h5: {
        fontFamily,
        fontSize: 16,
      },
      h6: {
        fontFamily,
        fontSize: 14,
      },
    },
  };
};

export const ColorModeContext = createContext({
  toggleColorMode: () => {},
});

export const useMode = () => {
  const [mode, setMode] = useState("dark");

  const colorMode = useMemo(
    () => ({
      toggleColorMode: () =>
        setMode((prev) => (prev === "light" ? "dark" : "light")),
    }),
    []
  );

  const theme = useMemo(() => createTheme(themeSettings(mode)), [mode]);
  return [theme, colorMode];
};
