import { Routes, Route } from "react-router-dom";
import Sidebar from "./scenes/global/Sidebar";
import Validator from "./scenes/validator/Index";
import FAQ from "./scenes/faq/Index";
import Playbook from "./scenes/playbookBuilder/Index";
import PlaybookAnalyzer from "./scenes/playbookAnalyzer/Index";
import Modules from "./scenes/playbookBuilder/Modules";
import SelectedModules from "./scenes/playbookBuilder/SelectedModules";
import Keywords from "./scenes/playbookBuilder/Keywords";
import Inventory from "./scenes/inventoryBuilder/Index";
import Tutorial from "./scenes/tutorial/Index";
import SelectCollections from "./scenes/playbookBuilder/Collections";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Home from "./scenes/home/Index";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "./theme";
import {
  NewPlaybookParameterContextProvider,
  GeneratedContextProvider,
  TreeViewContextProvider,
  YamlViewContextProvider,
  AttributesPresentContextProvider,
  AttributesContextProvider,
  ShowButtonContextProvider,
  EditPressedContextProvider,
  AddPressedContextProvider,
  AddKeywordsPressedContextProvider,
  AddModulesPressedContextProvider,
  SelectedNodeIDContextProvider,
  SelectedNodeNameContextProvider,
  SelectedNodeTypeContextProvider,
} from "./scenes/playbookBuilder/PlaybookBuilderContexts";
import { SelectedProvider } from "./services/SelectedContext";
import { VisitedCollectionsContextProvider } from "./services/VisitedContext";

import "./index.css";

function App() {
  const [theme, colorMode] = useMode();

  const navigate = useNavigate();

  useEffect(() => {
    navigate("/");
  }, []);

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <SelectedProvider>
          <NewPlaybookParameterContextProvider>
            <VisitedCollectionsContextProvider>
              <TreeViewContextProvider>
                <YamlViewContextProvider>
                  <ShowButtonContextProvider>
                    <EditPressedContextProvider>
                      <AddPressedContextProvider>
                        <AddKeywordsPressedContextProvider>
                          <AddModulesPressedContextProvider>
                            <SelectedNodeIDContextProvider>
                              <SelectedNodeNameContextProvider>
                                <SelectedNodeTypeContextProvider>
                                  <AttributesPresentContextProvider>
                                    <AttributesContextProvider>
                                      <GeneratedContextProvider>
                                        <CssBaseline />
                                        <div className="app">
                                          <Sidebar />
                                          <div className="content-wrapper">
                                            <main className="content">
                                              <Routes>
                                                <Route
                                                  path="/tutorial"
                                                  element={<Tutorial />}
                                                />
                                                <Route
                                                  path="/playbook_analyzer"
                                                  element={<PlaybookAnalyzer />}
                                                />
                                                <Route
                                                  path="/"
                                                  element={<Home />}
                                                />
                                                <Route
                                                  path="/validator"
                                                  element={<Validator />}
                                                />
                                                <Route
                                                  path="/inventory"
                                                  element={<Inventory />}
                                                />
                                                <Route
                                                  path="/playbook"
                                                  element={<Playbook />}
                                                />
                                                <Route
                                                  path="/selectCollections"
                                                  element={
                                                    <SelectCollections />
                                                  }
                                                />
                                                <Route
                                                  path="/collections/:id"
                                                  element={<Modules />}
                                                />
                                                <Route
                                                  path="/keywords"
                                                  element={<Keywords />}
                                                />
                                                <Route
                                                  path="/collections/:id/selectedModules"
                                                  element={<SelectedModules />}
                                                />
                                                {/* <Route path="/network" element={<MoveBoxes />} /> */}
                                                <Route
                                                  path="/FAQ"
                                                  element={<FAQ />}
                                                />
                                                <Route
                                                  path="*"
                                                  element={Home}
                                                />
                                              </Routes>
                                            </main>
                                          </div>
                                        </div>
                                      </GeneratedContextProvider>
                                    </AttributesContextProvider>
                                  </AttributesPresentContextProvider>
                                </SelectedNodeTypeContextProvider>
                              </SelectedNodeNameContextProvider>
                            </SelectedNodeIDContextProvider>
                          </AddModulesPressedContextProvider>
                        </AddKeywordsPressedContextProvider>
                      </AddPressedContextProvider>
                    </EditPressedContextProvider>
                  </ShowButtonContextProvider>
                </YamlViewContextProvider>
              </TreeViewContextProvider>
            </VisitedCollectionsContextProvider>
          </NewPlaybookParameterContextProvider>
        </SelectedProvider>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}

export default App;
