import React from "react";
import { render } from "@testing-library/react";
import { ThemeProvider } from "@mui/material/styles";
import {
  FloatingAce,
  MainBox,
  MainHeaderBox,
  LeftHeaderBox,
  GraphButtonBox,
} from "../../components/components";

describe("FloatingAce", () => {
  test("renders Box component with correct styles and position", () => {
    const isMobile = true;
    const theme = "dark";
    const showYAMLEditor = true;

    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        {showYAMLEditor && (
          <FloatingAce
            data-testid="floating-ace"
            sx={{
              top: isMobile ? "150px" : "80px",
              left: isMobile ? "20px" : "200px",
              right: isMobile ? "20px" : "50px",
              bottom: isMobile ? "20px" : "100px",
            }}
          ></FloatingAce>
        )}
      </ThemeProvider>
    );

    if (showYAMLEditor) {
      const floatingAce = getByTestId("floating-ace");
      expect(floatingAce).toBeInTheDocument();
      expect(floatingAce).toHaveStyle({
        position: "absolute",
        top: isMobile ? "150px" : "80px",
        left: isMobile ? "20px" : "200px",
        right: isMobile ? "20px" : "50px",
        bottom: isMobile ? "20px" : "100px",
      });
    }
  });
});

describe("MainBox", () => {
  test("renders correctly with provided props", () => {
    const isMobile = true;
    const theme = "dark";
    const justifyContent = "space-between";

    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <MainBox
          data-testid="main-box"
          justifyContent={justifyContent}
          sx={{
            marginBottom: 0,
            flexDirection: isMobile ? "column" : "row",
          }}
        ></MainBox>
      </ThemeProvider>
    );

    if (isMobile) {
      const mainBox = getByTestId("main-box");
      expect(mainBox).toBeInTheDocument();
      expect(mainBox).toHaveStyle({
        marginBottom: 0,
        flexDirection: isMobile ? "column" : "row",
      });
    }
  });
});

describe("MainHeaderBox", () => {
  test("renders correctly with provided props", () => {
    const theme = "dark";

    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <MainHeaderBox
          data-testid="main-header-box"
          sx={{
            paddingBottom: 0,
          }}
        ></MainHeaderBox>
      </ThemeProvider>
    );

    if (theme) {
      const mainHeaderBox = getByTestId("main-header-box");
      expect(mainHeaderBox).toBeInTheDocument();
      expect(mainHeaderBox).toHaveStyle({
        paddingBottom: 0,
      });
    }
  });
});

describe("LeftHeaderBox", () => {
  test("renders correctly with provided props", () => {
    const theme = "dark";

    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <LeftHeaderBox
          data-testid="left-header-box"
          sx={{
            display: "flex",
          }}
        ></LeftHeaderBox>
      </ThemeProvider>
    );

    if (theme) {
      const leftHeaderBox = getByTestId("left-header-box");
      expect(leftHeaderBox).toBeInTheDocument();
      expect(leftHeaderBox).toHaveStyle({
        display: "flex",
      });
    }
  });
});

describe("GraphButtonBox", () => {
  test("renders correctly with provided props", () => {
    const theme = "dark";
    const styles = {
      display: "flex",
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center",
    };

    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <GraphButtonBox
          data-testid="graph-button-box"
          sx={{
            styles,
          }}
        ></GraphButtonBox>
      </ThemeProvider>
    );

    if (theme) {
      const graphButtonBox = getByTestId("graph-button-box");
      expect(graphButtonBox).toBeInTheDocument();
      expect(graphButtonBox).toHaveStyle({
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
      });
    }
  });
});
