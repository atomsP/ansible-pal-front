import React, { useState, useContext, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import Tree from "react-d3-tree";
import { useMediaQuery } from "react-responsive";

import { Box, Button, useTheme } from "@mui/material";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import MainInstructions from "../global/instructions/MainInstructions";
import "./styles.css";

import {
  NewPlaybookParameterContext,
  GeneratedContext,
  EditPressedContext,
  AddPressedContext,
  ShowButtonContext,
  AddKeywordsPressedContext,
  AddModulesPressedContext,
  SelectedNodeIDContext,
  SelectedNodeNameContext,
  SelectedNodeTypeContext,
} from "../playbookBuilder/PlaybookBuilderContexts";

import { translateToAceFormat } from "./functions/translateToAceFormat";
import "./styles.css";
import "../global/ace.css";
import Header from "../../components/Header";
import { generatePlaybook } from "./functions/generatePlaybook";
import RenderSvgNode from "./RenderSvg";

import {
  FloatingAce,
  MainEnvelope,
  MainHeaderBox,
  LeftHeaderBox,
  GraphButtonBox,
  ConfirmationDialog,
  ConfirmationModal,
  AceWindow,
  pulsingAnimation,
  PopoverTypography,
  PopoverComponent,
} from "../../components/components";
import MessageSnackbar from "../global/Snackbar";

const Playbook = () => {
  const { setShowButton } = useContext(ShowButtonContext);
  const { isAddPressed } = useContext(AddPressedContext);
  const { isEditPressed } = useContext(EditPressedContext);
  const { isAddKeywordsPressed, setIsAddKeywordsPressed } = useContext(
    AddKeywordsPressedContext
  );
  const { isAddModulesPressed, setIsAddModulesPressed } = useContext(
    AddModulesPressedContext
  );

  const { selectedNodeName } = useContext(SelectedNodeNameContext);
  const { selectedNodeId } = useContext(SelectedNodeIDContext);
  const { selectedNodeType } = useContext(SelectedNodeTypeContext);
  const [generating, setGenerating] = useState(false);
  const [isClearAllConfirmationOpen, setIsClearAllConfirmationOpen] =
    useState(false);

  const [message, setMessage] = useState(null);
  const [messageType, setMessagetype] = useState(null);

  const theme = useTheme();
  const navigate = useNavigate();

  const { generated, setGenerated } = useContext(GeneratedContext);
  const { newPlaybookParameters, resetContext } = useContext(
    NewPlaybookParameterContext
  );

  const [anchorEl, setAnchorEl] = useState(null);
  const [showEditor, setShowEditor] = useState(false);
  const [showYAMLEditor, setShowYAMLEditor] = useState(false);

  const elementRef = useRef(null);
  const [width, setWidth] = useState(0);
  const [height, setHeight] = useState(0);
  // const isTablet = useMediaQuery({ minWidth: 768, maxWidth: 1023 });
  const isMobile = useMediaQuery({ maxWidth: 767 });

  const handleSnackbarClose = () => {
    setMessage(null);
  };

  const handlePreviewClick = () => {
    if (!showYAMLEditor) {
      setShowEditor(!showEditor);
    }
  };
  const handleYAMLPreviewClick = () => {
    if (!showEditor) {
      setShowYAMLEditor(!showYAMLEditor);
    }
  };

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  const generatingClose = () => setGenerating(false);

  const handleClick = (e) => {
    const tagName = e.target.tagName.toLowerCase();
    if (tagName !== "select") {
      setShowButton((prev) => ({
        ...prev,
        [selectedNodeId]: false,
      }));
    }
  };

  const translate = {
    x: width / 2,
    y: height / 2.5,
  };

  useEffect(() => {
    setIsAddModulesPressed(false);
    setIsAddKeywordsPressed(false);

    const element = elementRef.current;
    const handleResize = () => {
      if (element) {
        setWidth(element.offsetWidth);
        setHeight(element.offsetHeight);
      }
    };
    if (isAddKeywordsPressed) {
      navigate(
        `/keywords?parentName=${selectedNodeType}&parentId=${selectedNodeId}`
      );
    }
    if (isAddModulesPressed) {
      navigate(`/selectCollections?parentId=${selectedNodeId}`);
    }
    if (!isAddPressed && !isEditPressed) {
      window.addEventListener("click", handleClick);
    } else {
      window.removeEventListener("click", handleClick);
    }
    handleResize();
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("click", handleClick);
      window.removeEventListener("resize", handleResize);
    };
  }, [
    isAddKeywordsPressed,
    isAddModulesPressed,
    isAddPressed,
    isEditPressed,
    navigate,
    selectedNodeName,
    selectedNodeId,
    selectedNodeType,
  ]);

  const treeContainerStyles = {
    width: "100%",
    height: "80%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  };

  const aceCode = translateToAceFormat(newPlaybookParameters);

  const downloadPlaybook = () => {
    if (generated) {
      const content = generated.replace(/\\n/g, "\n").trim();
      const file = new Blob([content], { type: "text/plain; charset=utf-8" });
      const fileURL = URL.createObjectURL(file);
      const link = document.createElement("a");
      link.href = fileURL;
      link.setAttribute("download", "playbook.txt");
      document.body.appendChild(link);
      link.click();
    }
  };

  //----------------Components

  const PreviewButton = () => {
    return (
      <Button
        sx={{
          m: 1.5,
          borderRadius: 0,
          animation: `${
            showEditor ? `${pulsingAnimation(1.15)} 2s infinite` : "none"
          }`,
        }}
        size="small"
        variant="contained"
        color="info"
        onClick={handlePreviewClick}
      >
        <span style={{ fontFamily: "Courier New", letterSpacing: "1px" }}>
          [PREVIEW]
        </span>
      </Button>
    );
  };
  const ClearButton = () => {
    const handleClearClick = () => {
      if (!showEditor && !showYAMLEditor) {
        setIsClearAllConfirmationOpen(true);
      }
    };

    const handleConfirmClear = () => {
      setIsClearAllConfirmationOpen(false);
      setGenerated("");
      resetContext();
    };

    const handleClearAllCloseConfirmation = () => {
      setIsClearAllConfirmationOpen(false);
    };

    return (
      <>
        {newPlaybookParameters[0]?.children?.length > 0 || generated ? (
          <Button
            sx={{
              m: 1.5,
              borderRadius: 0,
            }}
            size="small"
            variant="contained"
            color="info"
            onClick={handleClearClick}
          >
            <span style={{ fontFamily: "Courier New", letterSpacing: "1px" }}>
              {isMobile ? "[CLEAR]" : "[CLEAR ALL]"}
            </span>
          </Button>
        ) : null}

        <ConfirmationDialog
          theme={theme}
          open={isClearAllConfirmationOpen}
          onClose={handleClearAllCloseConfirmation}
          title="Confirm"
          content="Are you sure you want to clear all data? This action cannot be
          undone."
          onClickNo={handleClearAllCloseConfirmation}
          onClickYes={handleConfirmClear}
        />
      </>
    );
  };
  const GeneratePlaybookButton = () => {
    return (
      <Button
        sx={{
          m: 1.5,
          borderRadius: 0,
        }}
        variant="contained"
        color="info"
        size="small"
        onClick={() => {
          if (!showEditor && !showYAMLEditor) {
            generatePlaybook({
              newPlaybookParameters,
              setGenerated,
              setGenerating,
              setMessage,
              setMessagetype,
            });
          }
        }}
      >
        <span style={{ fontFamily: "Courier New", letterSpacing: "1px" }}>
          {isMobile ? "[GENERATE]" : "[GENERATE Playbook]"}
        </span>
      </Button>
    );
  };
  const DownloadPlaybookButton = () => {
    return (
      <Button
        sx={{
          m: 1.5,
          borderRadius: 0,
        }}
        variant="contained"
        color="info"
        size="small"
        onClick={() => {
          if (!showEditor && !showYAMLEditor) {
            downloadPlaybook();
          }
        }}
      >
        <span style={{ fontFamily: "Courier New", letterSpacing: "1px" }}>
          {isMobile ? "[DOWNLOAD]" : "[DOWNLOAD Playbook]"}
        </span>
      </Button>
    );
  };

  const ViewYamlButton = () => {
    return (
      <Button
        sx={{
          m: 1.5,
          borderRadius: 0,
          fontFamily: "Courier New",
          letterSpacing: "1px",
          animation: `${
            showYAMLEditor ? `${pulsingAnimation} 2s infinite` : "none"
          }`,
        }}
        variant="contained"
        color="secondary"
        size="small"
        onClick={() => {
          handleYAMLPreviewClick();
        }}
      >
        {isMobile ? "[YAML]" : "[View Yaml]"}
      </Button>
    );
  };

  return (
    <>
      <MainEnvelope
        data-testid="main-box"
        // ref={mainBoxRef}
        justifyContent="space-between"
        sx={{
          marginBottom: 0,
          flexDirection: isMobile ? "column" : "row",
        }}
      >
        <MainHeaderBox data-testid="main-header-box" sx={{ paddingBottom: 0 }}>
          <LeftHeaderBox data-testid="left-header-box" sx={{ display: "flex" }}>
            <Header subtitle="Build Playbook" />
            <PopoverTypography
              open={open}
              handlePopoverOpen={handlePopoverOpen}
              handlePopoverClose={handlePopoverClose}
              icon={<InfoOutlinedIcon />}
              theme={theme}
            />
            <PopoverComponent
              open={open}
              anchorEl={anchorEl}
              handlePopoverClose={handlePopoverClose}
              text={<MainInstructions />}
            />
          </LeftHeaderBox>
        </MainHeaderBox>
        <Box>
          <GraphButtonBox data-testid="graph-button-box">
            <PreviewButton />
            {generated.length !== 0 && <ViewYamlButton />}
            {generated.length !== 0 && <DownloadPlaybookButton />}
            {newPlaybookParameters[0].children.length !== 0 && (
              <GeneratePlaybookButton />
            )}
            {(generated.length !== 0 ||
              newPlaybookParameters[0].children.length !== 0) && (
              <ClearButton />
            )}
            <ConfirmationModal
              open={generating}
              onClose={generatingClose}
              theme={theme}
              generated={generated}
            />
          </GraphButtonBox>

          {showEditor && (
            <FloatingAce
              data-testid="floating-ace"
              sx={{
                top: isMobile ? "150px" : "80px",
                left: isMobile ? "20px" : "200px",
                right: isMobile ? "20px" : "50px",
                bottom: isMobile ? "20px" : "100px",
              }}
            >
              <AceWindow theme={theme} value={aceCode} />
            </FloatingAce>
          )}
          {showYAMLEditor && (
            <FloatingAce
              data-testid="floating-ace"
              sx={{
                top: isMobile ? "150px" : "80px",
                left: isMobile ? "20px" : "200px",
                right: isMobile ? "20px" : "50px",
                bottom: isMobile ? "20px" : "100px",
              }}
            >
              <AceWindow theme={theme} value={generated} />
            </FloatingAce>
          )}
        </Box>
      </MainEnvelope>
      <div ref={elementRef} style={treeContainerStyles}>
        <Tree
          data={newPlaybookParameters}
          translate={translate}
          orientation={"vertical"}
          collapsible={false}
          pathFunc="elbow"
          rootNodeClassName="node__root"
          branchNodeClassName="node__branch"
          leafNodeClassName="node__leaf"
          renderCustomNodeElement={({ nodeDatum }) => (
            <RenderSvgNode nodeDatum={nodeDatum} />
          )}
        />
        <MessageSnackbar
          open={message}
          message={message}
          onClose={handleSnackbarClose}
          messageType={messageType}
        />
      </div>
    </>
  );
};

export default Playbook;
