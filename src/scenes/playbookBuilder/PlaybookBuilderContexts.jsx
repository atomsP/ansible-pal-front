import React, { createContext, useState } from "react";

export const AddKeywordsPressedContext = createContext();

export const AddKeywordsPressedContextProvider = ({ children }) => {
  const [isAddKeywordsPressed, setIsAddKeywordsPressed] = useState(false);

  return (
    <AddKeywordsPressedContext.Provider
      value={{ isAddKeywordsPressed, setIsAddKeywordsPressed }}
    >
      {children}
    </AddKeywordsPressedContext.Provider>
  );
};

export const AddModulesPressedContext = createContext();

export const AddModulesPressedContextProvider = ({ children }) => {
  const [isAddModulesPressed, setIsAddModulesPressed] = useState(false);

  return (
    <AddModulesPressedContext.Provider
      value={{ isAddModulesPressed, setIsAddModulesPressed }}
    >
      {children}
    </AddModulesPressedContext.Provider>
  );
};

export const AddPressedContext = createContext();

export const AddPressedContextProvider = ({ children }) => {
  const [isAddPressed, setIsAddPressed] = useState(false);

  return (
    <AddPressedContext.Provider value={{ isAddPressed, setIsAddPressed }}>
      {children}
    </AddPressedContext.Provider>
  );
};

export const AttributesContext = createContext();

export const AttributesContextProvider = ({ children }) => {
  const [attributes, setAttributes] = useState(null);

  return (
    <AttributesContext.Provider value={{ attributes, setAttributes }}>
      {children}
    </AttributesContext.Provider>
  );
};

export const AttributesPresentContext = createContext();

export const AttributesPresentContextProvider = ({ children }) => {
  const [isAttributesPresent, setIsAttributesPresent] = useState(true);

  return (
    <AttributesPresentContext.Provider
      value={{ isAttributesPresent, setIsAttributesPresent }}
    >
      {children}
    </AttributesPresentContext.Provider>
  );
};

export const EditPressedContext = createContext();

export const EditPressedContextProvider = ({ children }) => {
  const [isEditPressed, setIsEditPressed] = useState(false);

  return (
    <EditPressedContext.Provider value={{ isEditPressed, setIsEditPressed }}>
      {children}
    </EditPressedContext.Provider>
  );
};

export const GeneratedContext = createContext();

export const GeneratedContextProvider = ({ children }) => {
  const [generated, setGenerated] = useState([]);

  return (
    <GeneratedContext.Provider
      value={{
        generated,
        setGenerated,
      }}
    >
      {children}
    </GeneratedContext.Provider>
  );
};

export const NewPlaybookParameterContext = createContext();

export const NewPlaybookParameterContextProvider = ({ children }) => {
  const initialPlaybookParameters = [
    {
      id: "Play",
      hosts: "all",
      name: "",
      attributes: {
        keywords: {},
        parameters: {},
      },
      children: [],
    },
  ];

  const [newPlaybookParameters, setNewPlaybookParameters] = useState(
    initialPlaybookParameters
  );

  const resetContext = () => {
    setNewPlaybookParameters(initialPlaybookParameters);
  };

  return (
    <NewPlaybookParameterContext.Provider
      value={{
        newPlaybookParameters,
        setNewPlaybookParameters,
        resetContext,
      }}
    >
      {children}
    </NewPlaybookParameterContext.Provider>
  );
};

export const SelectedContext = createContext();

export const SelectedProvider = ({ children }) => {
  const [selected, setSelected] = useState("");

  return (
    <SelectedContext.Provider value={{ selected, setSelected }}>
      {children}
    </SelectedContext.Provider>
  );
};

export const SelectedNodeIDContext = createContext();

export const SelectedNodeIDContextProvider = ({ children }) => {
  const [selectedNodeId, setSelectedNodeId] = useState(null);

  return (
    <SelectedNodeIDContext.Provider
      value={{ selectedNodeId, setSelectedNodeId }}
    >
      {children}
    </SelectedNodeIDContext.Provider>
  );
};

export const SelectedNodeNameContext = createContext();

export const SelectedNodeNameContextProvider = ({ children }) => {
  const [selectedNodeName, setSelectedNodeName] = useState(null);

  return (
    <SelectedNodeNameContext.Provider
      value={{ selectedNodeName, setSelectedNodeName }}
    >
      {children}
    </SelectedNodeNameContext.Provider>
  );
};

export const SelectedNodeTypeContext = createContext();

export const SelectedNodeTypeContextProvider = ({ children }) => {
  const [selectedNodeType, setSelectedNodeType] = useState("");

  return (
    <SelectedNodeTypeContext.Provider
      value={{ selectedNodeType, setSelectedNodeType }}
    >
      {children}
    </SelectedNodeTypeContext.Provider>
  );
};

export const ShowButtonContext = createContext();

export const ShowButtonContextProvider = ({ children }) => {
  const [showButton, setShowButton] = useState({});

  return (
    <ShowButtonContext.Provider value={{ showButton, setShowButton }}>
      {children}
    </ShowButtonContext.Provider>
  );
};

export const ToggleAceContext = createContext();

export const ToggleAceContextProvider = ({ children }) => {
  const [isYamlAce, setYamlAce] = useState(false);

  return (
    <ToggleAceContext.Provider
      value={{
        isYamlAce,
        setYamlAce,
      }}
    >
      {children}
    </ToggleAceContext.Provider>
  );
};

export const TreeViewContext = createContext();

export const TreeViewContextProvider = ({ children }) => {
  const [isTreeView, setIsTreeView] = useState(true);

  return (
    <TreeViewContext.Provider
      value={{
        isTreeView,
        setIsTreeView,
      }}
    >
      {children}
    </TreeViewContext.Provider>
  );
};

export const YamlViewContext = createContext();

export const YamlViewContextProvider = ({ children }) => {
  const [isYamlView, setIsYamlView] = useState(false);

  return (
    <YamlViewContext.Provider
      value={{
        isYamlView,
        setIsYamlView,
      }}
    >
      {children}
    </YamlViewContext.Provider>
  );
};
