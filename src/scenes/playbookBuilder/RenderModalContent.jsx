import React, { useState, useContext, useEffect } from "react";
import { useMediaQuery } from "react-responsive";
import Typography from "@mui/material/Typography";
import { Box, useTheme } from "@mui/material";
import { findNode } from "./functions/findNode";

import {
  NewPlaybookParameterContext,
  AttributesContext,
  AttributesPresentContext,
} from "../playbookBuilder/PlaybookBuilderContexts";

import { ConfirmationDialog } from "../../components/components";

export const RenderModalContent = React.forwardRef(({ node, nodeId }, ref) => {
  const [selectedItem, setSelectedItem] = useState(null);
  const [isConfirmationOpen, setIsConfirmationOpen] = useState(false);
  const { attributes, setAttributes } = useContext(AttributesContext);
  const isTablet = useMediaQuery({ minWidth: 768, maxWidth: 1023 });
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const { newPlaybookParameters, setNewPlaybookParameters, resetContext } =
    useContext(NewPlaybookParameterContext);
  const { isAttributesPresent, setIsAttributesPresent } = useContext(
    AttributesPresentContext
  );
  const theme = useTheme();

  useEffect(() => {
    const node = findNode(newPlaybookParameters, nodeId);
    setAttributes(node.attributes);
    if (
      (node.id === "Play" &&
        Object.keys(node.attributes.keywords).length === 0) ||
      ((!node.attributes.parameters ||
        node.attributes.parameters.length === 0) &&
        (!node.attributes.keywords ||
          Object.keys(node.attributes.keywords).length === 0) &&
        (!node.roles || node.roles.length === 0))
    ) {
      setIsAttributesPresent(false);
    } else {
      setIsAttributesPresent(true);
    }
  }, [newPlaybookParameters]);

  const modalStyle = {
    position: "absolute",
    left: isMobile ? "5px" : "50%",
    right: isMobile ? "5px" : "50%",
    top: "50%",
    transform: "translate(-50%, -50%)",
    width: isMobile ? "90vw" : isTablet ? "50vw" : "40vw",
    bgcolor: theme.palette.modal.background,
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  const handleRemoveParameter = (parameterName, nodes) => {
    return nodes.map((node) => {
      if (node.id === nodeId) {
        const updatedAttributes = { ...node.attributes };

        if (Array.isArray(updatedAttributes.parameters)) {
          updatedAttributes.parameters = updatedAttributes.parameters.filter(
            (parameter) => parameter.name !== parameterName
          );
        }

        if (updatedAttributes.keywords) {
          const updatedKeywords = { ...updatedAttributes.keywords };

          if (updatedKeywords.hasOwnProperty(parameterName)) {
            delete updatedKeywords[parameterName];
          }

          updatedAttributes.keywords = updatedKeywords;
        }

        return { ...node, attributes: updatedAttributes };
      }

      if (Array.isArray(node.children)) {
        const updatedChildren = handleRemoveParameter(
          parameterName,
          node.children
        );
        return { ...node, children: updatedChildren };
      }

      return node;
    });
  };

  const handleParameterValueChange = (parameterName, newValue, nodes) => {
    return nodes.map((node) => {
      if (node.id === nodeId) {
        const updatedAttributes = { ...node.attributes };

        if (Array.isArray(updatedAttributes.parameters)) {
          updatedAttributes.parameters = updatedAttributes.parameters.map(
            (parameter) => {
              if (parameter.name === parameterName) {
                return { ...parameter, value: newValue };
              }
              return parameter;
            }
          );
        }

        return { ...node, attributes: updatedAttributes };
      }

      if (Array.isArray(node.children)) {
        const updatedChildren = handleParameterValueChange(
          parameterName,
          newValue,
          node.children
        );
        return { ...node, children: updatedChildren };
      }

      return node;
    });
  };

  const handleKeywordValueChange = (keywordName, newValue, nodes) => {
    return nodes.map((node) => {
      if (node.id === nodeId) {
        const updatedAttributes = { ...node.attributes };

        if (
          updatedAttributes.keywords &&
          updatedAttributes.keywords[keywordName]
        ) {
          updatedAttributes.keywords = {
            ...updatedAttributes.keywords,
            [keywordName]: newValue,
          };
        } else {
          updatedAttributes.keywords = {
            ...updatedAttributes.keywords,
            [keywordName]: newValue,
          };
        }

        return { ...node, attributes: updatedAttributes };
      }

      if (Array.isArray(node.children)) {
        const updatedChildren = handleKeywordValueChange(
          keywordName,
          newValue,
          node.children
        );
        return { ...node, children: updatedChildren };
      }

      return node;
    });
  };

  const handleRemoveParameterClick = (parameterName) => {
    setNewPlaybookParameters((prevParameters) => {
      const updatedParameters = handleRemoveParameter(
        parameterName,
        prevParameters
      );
      return updatedParameters;
    });
  };

  const handleRoleNameChange = (roleType, newName) => {
    setNewPlaybookParameters((prevParameters) => {
      const updatedParameters = [...prevParameters];
      const node = findNode(updatedParameters, nodeId);

      if (node) {
        const role = node.roles.find((r) => r.roleType === roleType);
        if (role) {
          role.roleName = newName;
        }
      }

      return updatedParameters;
    });
  };

  const handleRemoveRoleClick = (roleType) => {
    setNewPlaybookParameters((prevParameters) => {
      const updatedParameters = [...prevParameters];
      const node = findNode(updatedParameters, nodeId);

      if (node) {
        node.roles = node.roles.filter((role) => role.roleType !== roleType);
      }

      return updatedParameters;
    });
  };

  const handleParameterValueChangeClick = (parameterName, newValue) => {
    setNewPlaybookParameters((prevParameters) => {
      const updatedParameters = handleParameterValueChange(
        parameterName,
        newValue,
        prevParameters
      );
      return updatedParameters;
    });
  };

  const handleKeywordValueChangeClick = (keywordName, newValue) => {
    setNewPlaybookParameters((prevParameters) => {
      const updatedParameters = handleKeywordValueChange(
        keywordName,
        newValue !== undefined ? newValue : "",
        prevParameters
      );
      return updatedParameters;
    });
  };

  const handleClearClick = (type, item) => {
    setSelectedItem({ type, item });
    setIsConfirmationOpen(true);
  };

  const handleConfirmClear = (key) => {
    setIsConfirmationOpen(false);
    if (selectedItem.type === "parameter") {
      handleRemoveParameterClick(key);
    } else if (selectedItem.type === "keyword") {
      handleRemoveParameterClick(key);
    } else if (selectedItem.type === "role") {
      handleRemoveRoleClick(key);
    }
  };
  const handleCloseConfirmation = () => {
    setIsConfirmationOpen(false);
  };

  return (
    <Box sx={modalStyle}>
      {!isAttributesPresent ? (
        <Typography id="keep-mounted-modal-title" variant="h6" component="h2">
          {"Nothing to show"}
        </Typography>
      ) : (
        <>
          <Typography id="keep-mounted-modal-title" variant="h6" component="h2">
            {"Type: " + node.type} <br />
            {"Name: " +
              (node.name?.length === 0
                ? "There is none but maybe you should think about it"
                : node.name)}
          </Typography>

          <Box sx={{ maxHeight: "400px", overflowY: "auto" }}>
            {attributes.parameters && attributes.parameters.length !== 0 && (
              <>
                <Typography
                  id="keep-mounted-modal-title"
                  variant="h6"
                  component="h2"
                >
                  Parameters:
                </Typography>
                <Typography
                  id="keep-mounted-modal-description"
                  sx={{ mt: 2 }}
                />
                <ul>
                  {Array.isArray(attributes.parameters) &&
                    attributes.parameters.map((parameter) => (
                      <li key={parameter.name}>
                        <strong>Collection:</strong> {parameter.collectionName}{" "}
                        <br />
                        <strong>Module:</strong> {parameter.moduleName} <br />
                        <strong>Parameter:</strong> {parameter.name} <br />
                        {parameter.type === "boolean" ? (
                          <>
                            <Box
                              style={{
                                display: "flex",
                                justifyContent: "start",
                              }}
                            >
                              <select
                                value={parameter.value}
                                onChange={(e) => {
                                  const newValue = e.target.value;
                                  handleParameterValueChangeClick(
                                    parameter.name,
                                    newValue
                                  );
                                }}
                              >
                                <option value="true">True</option>
                                <option value="false">False</option>
                              </select>
                              <button
                                onClick={() =>
                                  handleClearClick("parameter", parameter.name)
                                }
                              >
                                Remove
                              </button>
                            </Box>
                            <br />
                          </>
                        ) : (
                          <>
                            <Box
                              style={{
                                display: "flex",
                                justifyContent: "start",
                              }}
                            >
                              <input
                                type="text"
                                value={parameter.value}
                                onChange={(e) => {
                                  const newValue = e.target.value;
                                  handleParameterValueChangeClick(
                                    parameter.name,
                                    newValue
                                  );
                                }}
                              />
                              <button
                                onClick={() =>
                                  handleClearClick("parameter", parameter.name)
                                }
                              >
                                Remove
                              </button>
                            </Box>
                            <br />
                          </>
                        )}
                      </li>
                    ))}
                </ul>
              </>
            )}

            {Object.keys(attributes.keywords ?? {}).length !== 0 && (
              <>
                <Typography
                  id="keep-mounted-modal-title"
                  variant="h6"
                  component="h2"
                >
                  Keywords:
                </Typography>
                <Typography
                  id="keep-mounted-modal-description"
                  sx={{ mt: 2 }}
                />
                <ul>
                  {attributes.keywords &&
                    Object.entries(attributes.keywords).map(([key, value]) => (
                      <li key={key}>
                        <Box>
                          <strong>{key}:</strong>
                          <br />
                          <Box
                            style={{
                              display: "flex",
                              justifyContent: "start",
                            }}
                          >
                            <input
                              type="text"
                              value={value}
                              onChange={(e) => {
                                const newValue = e.target.value;
                                if (newValue !== undefined) {
                                  handleKeywordValueChangeClick(key, newValue);
                                }
                              }}
                            />
                            <button
                              onClick={() => handleClearClick("keyword", key)}
                            >
                              Remove
                            </button>
                          </Box>
                          <br />
                        </Box>
                      </li>
                    ))}
                </ul>
              </>
            )}

            {node.roles && node.roles.length !== 0 && (
              <>
                <Typography
                  id="keep-mounted-modal-title"
                  variant="h6"
                  component="h2"
                >
                  Roles:
                </Typography>
                <Typography
                  id="keep-mounted-modal-description"
                  sx={{ mt: 2 }}
                />
                <ul>
                  {node.roles.map((role) => (
                    <li key={role.roleType}>
                      {role.roleType && (
                        <div>
                          <strong>Role Type:</strong> {role.roleType} <br />
                        </div>
                      )}
                      <Box
                        style={{
                          display: "flex",
                          justifyContent: "start",
                        }}
                      >
                        <input
                          type="text"
                          value={role.roleName}
                          onChange={(e) =>
                            handleRoleNameChange(role.roleType, e.target.value)
                          }
                        />{" "}
                        <button
                          onClick={() =>
                            handleClearClick("role", role.roleType)
                          }
                        >
                          Remove
                        </button>
                      </Box>
                      <br />
                    </li>
                  ))}
                </ul>
              </>
            )}
            <ConfirmationDialog
              theme={theme}
              open={isConfirmationOpen}
              onClose={handleCloseConfirmation}
              title="Confirm Removal"
              content={`Really want to remove ${selectedItem?.item}? This playbook
                will be just not the same without it`}
              onClickNo={handleCloseConfirmation}
              onClickYes={() =>
                handleConfirmClear(selectedItem?.item || selectedItem)
              }
            />
          </Box>
        </>
      )}
    </Box>
  );
});
