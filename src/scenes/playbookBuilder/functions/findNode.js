export const findNode = (nodes, nodeId) => {
  for (let node of nodes) {
    if (node.id === nodeId) {
      return node;
    }
    if (node.children) {
      const foundNode = findNode(node.children, nodeId);
      if (foundNode) {
        return foundNode;
      }
    }
  }
  return null;
};
