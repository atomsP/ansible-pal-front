import axios from "axios";

export const generatePlaybook = ({
  newPlaybookParameters,
  setGenerated,
  setGenerating,
  setMessage,
  setMessagetype,
}) => {
  axios
    .post(
      `http://193.219.91.103:8000/generate-playbook/`,

      {
        data: newPlaybookParameters,
      },
      { timeout: 1500 }
    )
    .then((response) => {
      setGenerated(response.data.data);
      setGenerating(true);
    })
    .catch((error) => {
      if (error.response) {
        const errorMessage = `Server responded with an error: ${error.response.data}`;
        setMessage(errorMessage);
        setMessagetype("error");
      } else if (error.request) {
        const errorMessage = "No response received from the server";
        setMessage(errorMessage);
        setMessagetype("error");
      } else {
        const errorMessage = `Error occurred while sending the request: ${error.message}`;
        setMessage(errorMessage);
        setMessagetype("error");
      }
    });
};
