export function formatObject(object, indent = 0) {
  const connector = "├──";
  const straightConnector = "│";
  const endConnector = "└──";
  const indentStr = " ".repeat(indent * 4);
  let result = "";
  for (const key in object) {
    if (object.hasOwnProperty(key)) {
      const formattedKey =
        key !== Object.keys(object).pop()
          ? `${connector} ${key}`
          : `${endConnector} ${key}`;
      const value = object[key];
      const formattedValue =
        typeof value === "object" && value !== null
          ? formatObject(value, indent + 1)
          : JSON.stringify(value);
      if (indent == 0) {
        result += `\n${indentStr}${formattedKey}: ${formattedValue}`;
      } else if (indent == 1) {
        result += `\n${straightConnector}${indentStr}${formattedKey}: ${formattedValue}`;
      } else {
        result += `\n${straightConnector}    ${straightConnector}   ${formattedKey}: ${formattedValue}`;
      }
    }
  }
  return result;
}
