export function translateToAceFormat(data) {
  let aceCode = "";

  function traverse(node, indent = "") {
    const hasAttributes =
      node.attributes && Object.keys(node.attributes).length > 0;
    const hasKeywords =
      node.attributes &&
      node.attributes.keywords &&
      Object.keys(node.attributes.keywords).length > 0;
    const hasParameters =
      node.attributes &&
      node.attributes.parameters &&
      node.attributes.parameters.length > 0;
    const hasRoles = node.roles && node.roles.length > 0;

    node.type === undefined
      ? (aceCode += `${indent}- ${node.name}`)
      : (aceCode += `${indent}- ${node.name}-${node.type}`);

    if (hasAttributes || hasKeywords || hasParameters || hasRoles) {
      const attributeIndent = `${indent}  `;

      if (hasKeywords) {
        aceCode += `\n${attributeIndent}${attributeIndent}keywords:`;
        const keywordIndent = `${attributeIndent}${attributeIndent}${attributeIndent}`;
        const keywordString = Object.entries(node.attributes.keywords)
          .map(([key, value]) => `${keywordIndent}${key}: ${value}`)
          .join(",\n");
        aceCode += `\n${keywordString}`;
      }

      if (hasParameters) {
        aceCode += `\n${attributeIndent}${attributeIndent}parameters:`;
        const parameterIndent = `${attributeIndent}${attributeIndent}${attributeIndent}`;
        const parameterString = node.attributes.parameters
          .map((parameter) => {
            const roleName = parameter.roleName
              ? `roleName: ${parameter.roleName},\n`
              : "";
            return `${parameterIndent}${parameter.name}: ${parameter.value},\n${parameterIndent}${roleName}${parameterIndent}collectionName: ${parameter.collectionName},\n${parameterIndent}${parameterIndent}moduleName: ${parameter.moduleName}`;
          })
          .join(",\n");
        aceCode += `\n${parameterString}`;
      }

      if (hasRoles) {
        aceCode += `\n${attributeIndent}${attributeIndent}roles:`;
        const roleIndent = `${attributeIndent}${attributeIndent}${attributeIndent}`;
        const roleString = node.roles
          .map((role) => {
            const roleName = role.roleName
              ? `roleName: ${role.roleName},\n`
              : "";
            return `${roleIndent}${role.roleType},\n${roleIndent}${roleName}`;
          })
          .join(",\n");
        aceCode += `\n${roleString}`;
      }
    }

    aceCode += "\n";

    if (node.children && node.children.length > 0) {
      indent += "  ";
      for (const child of node.children) {
        traverse(child, indent);
      }
    }
  }

  traverse(data[0]);
  return aceCode;
}
