import React, { useContext, useState, useEffect } from "react";
import { useLocation, Link, useNavigate } from "react-router-dom";
import { Button, useTheme, Box } from "@mui/material";
import Typography from "@mui/material/Typography";
import { List } from "react-virtualized";
import { useMediaQuery } from "react-responsive";

import ArrowBackIosOutlinedIcon from "@mui/icons-material/ArrowBackIosOutlined";
import ClearOutlinedIcon from "@mui/icons-material/ClearOutlined";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";

import {
  NewPlaybookParameterContext,
  GeneratedContext,
  TreeViewContext,
  YamlViewContext,
  EditPressedContext,
  AddPressedContext,
} from "../playbookBuilder/PlaybookBuilderContexts";

import PreviewWindowInfoPopover from "../global/instructions/PreviewWindowInfoPopover";
import img from "../../assets/ansiblePlaypompo.png";
import Header from "../../components/Header";
import Body from "../../components/Body";
import MessageSnackbar from "../global/Snackbar";

import { translateToAceFormat } from "./functions/translateToAceFormat";
import { generatePlaybook } from "./functions/generatePlaybook";
import "../global/ace.css";

import {
  MainBox,
  ConfirmationModal,
  AceComponent,
  PopoverTypography,
  PopoverComponent,
  HeadingBox,
  HeaderAndPopoverBox,
  ImportedComponentBox,
} from "../../components/components";

const SelectedModules = () => {
  const [selectedModule, setSelectedModule] = useState(null);
  const [showInputField, setShowInputField] = useState({});
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [popoverParamComments, setPopoverParamComments] = React.useState("");
  const [generating, setGenerating] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");
  const [filteredParams, setFilteredParams] = useState([]);
  const [message, setMessage] = useState(null);
  const [messageType, setMessagetype] = useState(null);

  const theme = useTheme();

  const location = useLocation();
  const moduleDataResponses = location?.state?.moduleDataResponses || [];
  const state = location.state;
  const queryParams = new URLSearchParams(location.search);
  const parentId = queryParams.get("parentId");
  const collectionTitle = queryParams.get("collectionTitle");
  const collectionId = queryParams.get("collectionId");
  const navigate = useNavigate();

  const { generated, setGenerated } = useContext(GeneratedContext);
  const { isTreeView, setIsTreeView } = useContext(TreeViewContext);
  const { isYamlView, setIsYamlView } = useContext(YamlViewContext);
  const { newPlaybookParameters, setNewPlaybookParameters } = useContext(
    NewPlaybookParameterContext
  );

  const { setIsAddPressed } = useContext(AddPressedContext);
  const { setIsEditPressed } = useContext(EditPressedContext);

  const generatingOpen = () => setGenerating(true);
  const generatingClose = () => setGenerating(false);

  const isDesktop = useMediaQuery({ minWidth: 1128 });
  const isSmall = useMediaQuery({ maxWidth: 610 });
  const isMobile = useMediaQuery({ maxWidth: 767 });

  //--------------Popover

  const handlePopoverOpen = (event, paramComments) => {
    setPopoverParamComments(paramComments);
    setAnchorEl(event.currentTarget);
  };
  const handlePopoverClose = () => {
    setAnchorEl(null);
  };
  const popOpen = Boolean(anchorEl);

  //---------------------

  const handleModuleClick = (moduleData) => {
    setSelectedModule(moduleData);
  };

  const handleBackClick = () => {
    setSelectedModule(null);
  };

  const handleSnackbarClose = () => {
    setMessage(null);
  };

  //----------------------Main

  useEffect(() => {
    if (selectedModule && selectedModule.parameter) {
      const filtered = selectedModule.parameter.filter((param) => {
        return param.name.toLowerCase().includes(searchTerm.toLowerCase());
      });
      setFilteredParams(filtered);
    }
  }, [selectedModule, searchTerm]);

  //-------------------Show list

  const rowRenderer = ({ index, style }) => {
    const param = filteredParams[index];
    if (!param) {
      return null;
    }
    const paramComments = param.comments;
    const rowStyle = {
      ...style,
      // color: "transparent",
      cursor: "pointer",
      color: theme.palette.list.rowRenderer,
      borderBottom: "1px solid rgb(212, 212, 212)",
      paddingTop: 7,
    };
    return (
      <div key={index} style={rowStyle}>
        <Box
          sx={{ paddingRight: 2 }}
          display="flex"
          justifyContent="space-between"
        >
          <Box display="flex">
            <Box sx={{ marginRight: 1 }}>
              <PopoverTypography
                open={popOpen}
                handlePopoverOpen={(event) =>
                  handlePopoverOpen(event, paramComments)
                }
                handlePopoverClose={handlePopoverClose}
                icon={<InfoOutlinedIcon />}
                theme={theme}
              />
              <PopoverComponent
                open={popOpen}
                anchorEl={anchorEl}
                handlePopoverClose={handlePopoverClose}
                text={
                  <Box sx={{ padding: 2, maxWidth: 300 }}>
                    {popoverParamComments}
                  </Box>
                }
              />
            </Box>
            <Box
              onClick={() => {
                setShowInputField({
                  ...showInputField,
                  [param.name]: true,
                });
              }}
            >
              <Body other={param.name} />
            </Box>
          </Box>
          <Box display="flex">
            {showInputField[param.name] === true ? (
              <Box
                onClick={() => {
                  setShowInputField({
                    ...showInputField,
                    [param.name]: false,
                  });
                  removeRecordByParamName(param.name);
                }}
              >
                <Body other=<ClearOutlinedIcon /> />
              </Box>
            ) : null}
            <Box sx={{ marginLeft: 1 }}>
              <Body subtitle={renderInputField(param)} />
            </Box>
          </Box>
        </Box>
      </div>
    );
  };

  const rowRendererTwo = ({ index, style }) => {
    const moduleData = moduleDataResponses[index];
    const rowStyle = {
      ...style,
      color: "transparent",
      cursor: "pointer",
      // color: "blue",
      paddingLeft: "11rem",
    };
    return (
      <div
        key={index}
        style={rowStyle}
        onClick={() => handleModuleClick(moduleData)}
      >
        <Box
          sx={{ paddingRight: 2 }}
          display="flex"
          justifyContent="space-between"
        >
          <Body other={moduleData.title} />
        </Box>
      </div>
    );
  };

  const renderInputField = (param) => {
    const getParentNode = (parameters) => {
      for (const parameter of parameters) {
        if (parameter.id === parentId) {
          return parameter;
        }
        if (parameter.children && parameter.children.length > 0) {
          const parentNode = getParentNode(parameter.children);
          if (parentNode) {
            return parentNode;
          }
        }
        if (
          parameter.attributes &&
          parameter.attributes.parameters &&
          parameter.attributes.parameters.length > 0
        ) {
          const parentNode = getParentNode(parameter.attributes.parameters);
          if (parentNode) {
            return parentNode;
          }
        }
      }
      return null;
    };

    const findAttributes = (parentId, children, paramName) => {
      for (const child of children) {
        if (child.id === parentId) {
          if (
            child.attributes &&
            child.attributes.parameters &&
            child.attributes.parameters.some(
              (param) => param.name === paramName
            )
          ) {
            return true;
          }
        }
        if (child.children && child.children.length > 0) {
          const found = findAttributes(parentId, child.children, paramName);
          if (found) {
            return true;
          }
        }
      }
      return false;
    };

    const getParentAttributeValue = (attributeName, parameters) => {
      const parentNode = getParentNode(parameters);
      const parameter = parentNode?.attributes?.parameters?.find(
        (param) => param.name === attributeName
      );

      return parameter ? parameter.value : "";
    };
    if (param.data_type === "boolean") {
      return (
        <>
          {showInputField[param.name] && (
            <select
              value={getParentAttributeValue(param.name, newPlaybookParameters)}
              onChange={(e) =>
                handleInputChange(param.name, e.target.value, param.data_type)
              }
            >
              {!findAttributes(parentId, newPlaybookParameters, param.name) && (
                <option value="">Select</option>
              )}
              <option value="true">True</option>
              <option value="false">False</option>
            </select>
          )}
        </>
      );
    } else {
      return (
        <>
          {showInputField[param.name] && (
            <input
              type="text"
              value={getParentAttributeValue(param.name, newPlaybookParameters)}
              onChange={(e) =>
                handleInputChange(param.name, e.target.value, param.data_type)
              }
            />
          )}
        </>
      );
    }
  };

  const handleInputChange = (paramName, value, paramDataType) => {
    const updatedParameters = [...newPlaybookParameters];
    const findAndSetAttributes = (parentId, children) => {
      for (let i = 0; i < children.length; i++) {
        const child = children[i];
        if (child.id === parentId) {
          if (!child.attributes) {
            child.attributes = {};
          }
          if (!child.attributes.parameters) {
            child.attributes.parameters = [];
          }
          const parameterIndex = child.attributes.parameters.findIndex(
            (param) => param.name === paramName
          );
          if (parameterIndex !== -1) {
            child.attributes.parameters[parameterIndex].value = value;
          } else {
            child.attributes.parameters.push({
              name: paramName,
              value: value,
              type: paramDataType,
              collectionId: collectionId,
              collectionName: collectionTitle,
              moduleId: selectedModule.id,
              moduleName: selectedModule.title,
            });
          }
          return true;
        }
        if (child.children && child.children.length > 0) {
          const found = findAndSetAttributes(parentId, child.children);
          if (found) return true;
        }
      }
      return false;
    };

    findAndSetAttributes(parentId, updatedParameters);
    setNewPlaybookParameters(updatedParameters);
  };

  const removeRecordByParamName = (paramName) => {
    setNewPlaybookParameters((prevParameters) => {
      const updatedParameters = [...prevParameters];
      const removeParam = (parameters) => {
        for (let i = 0; i < parameters.length; i++) {
          const parameter = parameters[i];
          if (parameter.attributes && parameter.attributes.parameters) {
            if (Array.isArray(parameter.attributes.parameters)) {
              const parameterIndex = parameter.attributes.parameters.findIndex(
                (param) => param.name === paramName
              );
              if (parameterIndex !== -1) {
                parameter.attributes.parameters.splice(parameterIndex, 1);
              }
            }
          }
          if (parameter.children && parameter.children.length > 0) {
            removeParam(parameter.children);
          }
        }
      };
      removeParam(updatedParameters);
      return updatedParameters;
    });
  };

  const imgHeight = 250;
  const aceCode = translateToAceFormat(newPlaybookParameters);

  //-------------------Components

  const GeneratePlaybookButton = () => {
    return (
      <Button
        sx={{
          m: 1.5,
          borderRadius: 0,
        }}
        variant="contained"
        color="info"
        onClick={() => {
          generatePlaybook({
            newPlaybookParameters,
            setGenerated,
            setGenerating,
            setMessage,
            setMessagetype,
          });
        }}
      >
        <span style={{ fontFamily: "Courier New", letterSpacing: "1px" }}>
          {isMobile ? "[Generate]" : "[Generate Playbook]"}
        </span>
      </Button>
    );
  };

  const ClearParametersButton = () => {
    const handleBackClick = () => {
      setIsAddPressed(false);
      setIsEditPressed(false);
      navigate("/playbook");
    };
    return (
      <Button
        sx={{
          m: 1.5,
          borderRadius: 0,
          fontFamily: "Courier New",
          letterSpacing: "1px",
        }}
        variant="contained"
        color="secondary"
        onClick={handleBackClick}
      >
        {isMobile ? "[Back]" : "[Back to Workshop]"}
      </Button>
    );
  };
  const SwitchButton = () => {
    return (
      <Button
        disabled={generated.length === 0}
        sx={{ m: 1.5 }}
        color="info"
        size="small"
        onClick={() => {
          setIsTreeView(!isTreeView);
          setIsYamlView(!isYamlView);
        }}
      >
        Tree/yAML
      </Button>
    );
  };
  const Illustrate = () => {
    return (
      <img
        alt="ansible-flag"
        style={{ height: imgHeight, width: imgHeight * 1.62 }}
        src={img}
      />
    );
  };

  //----------------------------------------

  return (
    <MainBox>
      <HeadingBox>
        <Box
          p={2}
          sx={{
            display: "flex",
            flexDirection: "row",
          }}
        >
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              width: !isDesktop ? "15rem" : "16rem",
            }}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
              }}
            >
              <HeaderAndPopoverBox>
                <Header subtitle={state.title} />
              </HeaderAndPopoverBox>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                }}
              >
                {selectedModule ? (
                  <Typography
                    onClick={handleBackClick}
                    style={{ cursor: "pointer" }}
                  >
                    <ArrowBackIosOutlinedIcon
                      style={{
                        color: theme.palette.list.arrowBack,
                      }}
                    />
                  </Typography>
                ) : (
                  <Typography>
                    <Link
                      to={`/collections/${collectionId}?parentId=${parentId}&collectionTitle=${collectionTitle}&collectionId=${collectionId}`}
                    >
                      <ArrowBackIosOutlinedIcon
                        style={{
                          color: theme.palette.list.arrowBack,
                        }}
                      />
                    </Link>
                  </Typography>
                )}
              </Box>
            </Box>
          </Box>
          <Box>
            <Body
              title={
                selectedModule
                  ? isMobile
                    ? "Parameters"
                    : "Select Parameters"
                  : isMobile
                  ? "Module"
                  : "Select Module"
              }
            />
            {selectedModule && (
              <input
                type="text"
                placeholder="Search"
                value={searchTerm}
                onChange={(e) => setSearchTerm(e.target.value)}
              />
            )}
          </Box>
        </Box>
      </HeadingBox>
      <ImportedComponentBox>
        <Box
          sx={{
            display: "flex",
            flexDirection: !isDesktop ? "column" : "row",
            alignItems: !isDesktop ? "center" : undefined,
            justifyContent: !isDesktop ? "center" : undefined,
            width: "100%",
            height: "100%",
          }}
        >
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              width: isSmall ? "50%" : "100%",
            }}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
                height: "100%",
              }}
            >
              <List
                width={isSmall ? 400 : 450}
                height={400}
                rowCount={
                  selectedModule
                    ? selectedModule.parameter.length
                    : moduleDataResponses.length
                }
                rowHeight={40}
                rowRenderer={selectedModule ? rowRenderer : rowRendererTwo}
              />
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  paddingTop: !isDesktop ? "20px" : undefined,
                  paddingBottom: !isDesktop ? "10px" : undefined,
                }}
              >
                {newPlaybookParameters &&
                  Object.keys(newPlaybookParameters).length !== 0 && (
                    <>
                      <GeneratePlaybookButton />
                      <ConfirmationModal
                        open={generating}
                        onClose={generatingClose}
                        theme={theme}
                        generated={generated}
                      />
                      <ClearParametersButton />
                    </>
                  )}
              </Box>
            </Box>
          </Box>
          <Box>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                height: "23rem",
                width: "100vh",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                {newPlaybookParameters.length !== 0 ||
                generated.length !== 0 ? (
                  <>
                    <Box
                      sx={{
                        display: "flex",
                        flexDirection: "row",
                        paddingTop: isDesktop ? "3rem" : "10rem",
                      }}
                    >
                      <SwitchButton />
                      <PreviewWindowInfoPopover />
                    </Box>
                    {isYamlView && generated.length !== 0 ? (
                      <AceComponent theme={theme} value={generated} />
                    ) : (
                      <AceComponent theme={theme} value={aceCode} />
                    )}
                  </>
                ) : (
                  <Box>
                    <Illustrate />
                  </Box>
                )}
                <MessageSnackbar
                  open={message}
                  message={message}
                  onClose={handleSnackbarClose}
                  messageType={messageType}
                />
              </Box>
            </Box>
          </Box>
        </Box>
      </ImportedComponentBox>
    </MainBox>
  );
};

export default SelectedModules;
