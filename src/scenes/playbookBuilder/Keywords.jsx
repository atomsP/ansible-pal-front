import React, { useContext, useState, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useMediaQuery } from "react-responsive";

import { Button, useTheme, Box } from "@mui/material";
import Typography from "@mui/material/Typography";
import { List } from "react-virtualized";
import Modal from "@mui/material/Modal";
import ArrowBackIosOutlinedIcon from "@mui/icons-material/ArrowBackIosOutlined";
import ClearOutlinedIcon from "@mui/icons-material/ClearOutlined";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";

import { AxiosContext } from "../../services/AxiosContext";

import {
  NewPlaybookParameterContext,
  GeneratedContext,
  TreeViewContext,
  YamlViewContext,
  EditPressedContext,
  AddPressedContext,
} from "../playbookBuilder/PlaybookBuilderContexts";

import PreviewWindowInfoPopover from "../global/instructions/PreviewWindowInfoPopover";
import img from "../../assets/ansiblePlaypompo.png";
import Header from "../../components/Header";
import Body from "../../components/Body";
import { translateToAceFormat } from "./functions/translateToAceFormat";
import { generatePlaybook } from "./functions/generatePlaybook";
import "../global/ace.css";
import MessageSnackbar from "../global/Snackbar";

import {
  MainBox,
  PopoverComponent,
  PopoverTypography,
  ConfirmationModal,
  AceComponent,
  HeadingBox,
  HeaderAndPopoverBox,
  ImportedComponentBox,
} from "../../components/components";

const Keywords = () => {
  const [selectedModule] = useState(null);
  const [keywords, setKeywords] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const [showInputField, setShowInputField] = useState({});
  const [message, setMessage] = useState(null);
  const [messageType, setMessagetype] = useState(null);
  const { setIsAddPressed } = useContext(AddPressedContext);
  const { setIsEditPressed } = useContext(EditPressedContext);

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [popoverKeywordComments, setPopoverKeywordComments] =
    React.useState("");
  const [open, setOpen] = React.useState(false);
  const [generating, setGenerating] = useState(false);

  const theme = useTheme();

  const isDesktop = useMediaQuery({ minWidth: 1128 });
  const isSmall = useMediaQuery({ maxWidth: 610 });
  const isMobile = useMediaQuery({ maxWidth: 767 });

  const location = useLocation();

  const queryParams = new URLSearchParams(location.search);
  const parentId = queryParams.get("parentId");
  const parentName = queryParams.get("parentName");
  const navigate = useNavigate();

  const { generated, setGenerated } = useContext(GeneratedContext);
  const { isTreeView, setIsTreeView } = useContext(TreeViewContext);
  const { isYamlView, setIsYamlView } = useContext(YamlViewContext);
  const axiosContext = useContext(AxiosContext);
  const { newPlaybookParameters, setNewPlaybookParameters } = useContext(
    NewPlaybookParameterContext
  );

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const generatingOpen = () => setGenerating(true);
  const generatingClose = () => setGenerating(false);

  const handleSnackbarClose = () => {
    setMessage(null);
  };

  //--------------Popover

  const handlePopoverOpen = (event, keywordDescription) => {
    setPopoverKeywordComments(keywordDescription);
    setAnchorEl(event.currentTarget);
  };
  const handlePopoverClose = () => {
    setAnchorEl(null);
  };
  const popOpen = Boolean(anchorEl);

  //-----------------------

  useEffect(() => {
    let url;
    if (parentName === "Play") {
      url = "group_keywords/1/";
    } else if (parentName === "Role") {
      url = "group_keywords/2/";
    } else if (parentName === "Block") {
      url = "group_keywords/3/";
    } else if (parentName === "Task") {
      url = "group_keywords/4/";
    }
    if (url) {
      axiosContext.publicAxios
        .get(url, { timeout: 3500 })
        .then((response) => {
          setKeywords(response.data);
        })
        .catch((error) => {
          if (error.response) {
            const errorMessage = `Server responded with an error: ${error.response.data}`;
            setMessage(errorMessage);
            setMessagetype("error");
          } else if (error.request) {
            const errorMessage = "No response received from the server";
            setMessage(errorMessage);
            setMessagetype("error");
          } else {
            const errorMessage = `Error occurred while sending the request: ${error.message}`;
            setMessage(errorMessage);
            setMessagetype("error");
          }
        });
    }
  }, [parentId]);

  const handleSearchQueryChange = (event) => {
    setSearchQuery(event.target.value);
  };
  const filteredKeywords = keywords.filter((keyword) =>
    keyword.title.toLowerCase().includes(searchQuery.toLowerCase())
  );

  //-------------------Show list

  const rowRenderer = ({ index, style }) => {
    const keyword = filteredKeywords[index];
    const keywordDescription = keyword.description;
    const rowStyle = {
      ...style,
      // color: "transparent",
      cursor: "pointer",
      color: theme.palette.list.rowRenderer,
      borderBottom: "1px solid rgb(212, 212, 212)",
      paddingTop: 7,
    };
    return (
      <div key={index} style={rowStyle}>
        <Box
          sx={{ paddingRight: 2 }}
          display="flex"
          justifyContent="space-between"
        >
          <Box display="flex">
            <Box sx={{ marginRight: 1 }}>
              <PopoverTypography
                open={popOpen}
                handlePopoverOpen={(event) =>
                  handlePopoverOpen(event, keywordDescription)
                }
                handlePopoverClose={handlePopoverClose}
                icon={<InfoOutlinedIcon />}
                theme={theme}
              />
              <PopoverComponent
                open={popOpen}
                anchorEl={anchorEl}
                handlePopoverClose={handlePopoverClose}
                text={
                  <Box sx={{ padding: 2, maxWidth: 300 }}>
                    {popoverKeywordComments}
                  </Box>
                }
              />
            </Box>
            <Box
              onClick={() => {
                setShowInputField({
                  ...showInputField,
                  [keyword.title]: true,
                });
              }}
            >
              <Body other={keyword.title} />
            </Box>
          </Box>
          <Box display="flex">
            {showInputField[keyword.title] === true ? (
              <Box
                onClick={() => {
                  setShowInputField({
                    ...showInputField,
                    [keyword.title]: false,
                  });
                  removeRecordByParamName(keyword.title);
                }}
              >
                <Body other=<ClearOutlinedIcon /> />
              </Box>
            ) : null}
            <Box sx={{ marginLeft: 1 }}>
              <Body subtitle={renderInputField(keyword)} />
            </Box>
          </Box>
        </Box>
      </div>
    );
  };

  //----------------Show input

  const renderInputField = (kw) => {
    const getParentNode = (keywords) => {
      for (const keyword of keywords) {
        if (keyword.id === parentId) {
          return keyword;
        }
        if (keyword.children && keyword.children.length > 0) {
          const parentNode = getParentNode(keyword.children);
          if (parentNode) {
            return parentNode;
          }
        }
        if (
          keyword.attributes &&
          keyword.attributes.keywords &&
          Object.values(keyword.attributes.keywords).length > 0
        ) {
          const parentNode = getParentNode(
            Object.values(keyword.attributes.keywords)
          );
          if (parentNode) {
            return parentNode;
          }
        }
      }
      return null;
    };
    const getParentAttributeValue = (keywordName, keywords) => {
      const parentNode = getParentNode(keywords);
      return parentNode?.attributes?.keywords?.[keywordName] || "";
    };
    return (
      <>
        {showInputField[kw.title] && (
          <input
            type="text"
            value={getParentAttributeValue(kw.title, newPlaybookParameters)}
            onChange={(e) => handleInputChange(kw.title, e.target.value)}
          />
        )}
      </>
    );
  };

  //--------------Add input

  const handleInputChange = (keywordName, value) => {
    const updatedParameters = [...newPlaybookParameters];
    const findAndSetAttributes = (parentId, children) => {
      for (let i = 0; i < children.length; i++) {
        const child = children[i];
        if (child.id === parentId) {
          if (!child.attributes) {
            child.attributes = {};
          }
          if (!child.attributes.keywords) {
            child.attributes.keywords = {};
          }
          child.attributes.keywords[keywordName] = value;
          return true;
        }
        if (child.children && child.children.length > 0) {
          const found = findAndSetAttributes(parentId, child.children);
          if (found) return true;
        }
      }
      return false;
    };

    findAndSetAttributes(parentId, updatedParameters);
    setNewPlaybookParameters(updatedParameters);
  };

  //--------------------Remove input

  const removeRecordByParamName = (keywordName) => {
    setNewPlaybookParameters((prevParameters) => {
      const updatedParameters = [...prevParameters];
      const removeKeyword = (node) => {
        if (
          node.attributes &&
          node.attributes.keywords &&
          node.attributes.keywords[keywordName]
        ) {
          delete node.attributes.keywords[keywordName];
        }
        if (node.children && node.children.length > 0) {
          node.children.forEach(removeKeyword);
        }
      };
      updatedParameters.forEach(removeKeyword);
      return updatedParameters;
    });
  };

  const imgHeight = 250;

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: theme.palette.modal.background,
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  const aceCode = translateToAceFormat(newPlaybookParameters);

  //-------------------Components

  const InformationModal = () => {
    return (
      <Modal
        keepMounted
        open={open}
        onClose={handleClose}
        aria-labelledby="keep-mounted-modal-title"
        aria-describedby="keep-mounted-modal-description"
      >
        <Box sx={style}>
          <Typography id="keep-mounted-modal-title" variant="h6" component="h2">
            {selectedModule.title}
          </Typography>
          <Typography id="keep-mounted-modal-description" sx={{ mt: 2 }}>
            {selectedModule.description}
            <br />
            Synopsis: {selectedModule.synopsis}
            <br />
            Requirements: {selectedModule.requirement}
          </Typography>
        </Box>
      </Modal>
    );
  };
  const InfoButton = () => {
    return (
      <Button
        sx={{ m: 1.5 }}
        variant="contained"
        color="info"
        onClick={handleOpen}
      >
        Information About {selectedModule.title}
      </Button>
    );
  };
  const GeneratePlaybookButton = () => {
    return (
      <Button
        sx={{
          m: 1.5,
          borderRadius: 0,
        }}
        variant="contained"
        color="info"
        onClick={() => {
          generatePlaybook({
            newPlaybookParameters,
            setGenerated,
            setGenerating,
            setMessage,
            setMessagetype,
          });
        }}
      >
        <span style={{ fontFamily: "Courier New", letterSpacing: "1px" }}>
          {isMobile ? "[Generate]" : "[Generate Playbook]"}
        </span>
      </Button>
    );
  };

  const ClearParametersButton = () => {
    return (
      <Button
        sx={{
          m: 1.5,
          borderRadius: 0,
          fontFamily: "Courier New",
          letterSpacing: "1px",
        }}
        variant="contained"
        color="secondary"
        onClick={() => {
          setIsAddPressed(false);
          setIsEditPressed(false);
          navigate(`/playbook`);
        }}
      >
        {isMobile ? "[Back]" : "[Back to Workshop]"}
      </Button>
    );
  };

  const SwitchButton = () => {
    return (
      <Button
        disabled={generated.length === 0}
        sx={{ m: 1.5 }}
        color="info"
        size="small"
        onClick={() => {
          setIsTreeView(!isTreeView);
          setIsYamlView(!isYamlView);
        }}
      >
        Tree/yAML
      </Button>
    );
  };
  const Illustrate = () => {
    return (
      <img
        alt="ansible-flag"
        style={{ height: imgHeight, width: imgHeight * 1.62 }}
        src={img}
      />
    );
  };

  //----------------------------------------

  return (
    <MainBox>
      <HeadingBox>
        <Box
          p={2}
          sx={{
            display: "flex",
            flexDirection: "row",
          }}
        >
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              width: !isDesktop ? "15rem" : "16rem",
            }}
          >
            <HeaderAndPopoverBox>
              {isSmall ? (
                <Header subtitle={`${parentName} level`} />
              ) : (
                <Header subtitle={`Select keywords for ${parentName} level`} />
              )}
            </HeaderAndPopoverBox>
            <Typography>
              <ArrowBackIosOutlinedIcon style={{ visibility: "hidden" }} />
            </Typography>
          </Box>
          <Box>
            <Body title="Select Keywords" />
            <input
              type="text"
              value={searchQuery}
              onChange={handleSearchQueryChange}
              placeholder="Search"
            />
          </Box>
        </Box>
        {selectedModule && (
          <>
            <InfoButton />
            <InformationModal />
          </>
        )}
      </HeadingBox>
      <ImportedComponentBox>
        <Box
          sx={{
            display: "flex",
            flexDirection: !isDesktop ? "column" : "row",
            alignItems: !isDesktop ? "center" : undefined,
            justifyContent: !isDesktop ? "center" : undefined,
            width: "100%",
            height: "100%",
          }}
        >
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              width: isSmall ? "50%" : "100%",
            }}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
                height: "100%",
              }}
            >
              <List
                width={isSmall ? 400 : 450}
                height={400}
                rowCount={filteredKeywords.length}
                rowHeight={40}
                rowRenderer={rowRenderer}
              />
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  paddingTop: !isDesktop ? "20px" : undefined,
                  paddingBottom: !isDesktop ? "10px" : undefined,
                }}
              >
                {newPlaybookParameters &&
                  Object.keys(newPlaybookParameters).length !== 0 && (
                    <>
                      <GeneratePlaybookButton />
                      <ConfirmationModal
                        open={generating}
                        onClose={generatingClose}
                        theme={theme}
                        generated={generated}
                      />
                      <ClearParametersButton />
                    </>
                  )}
              </Box>
            </Box>
          </Box>
          <Box>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                height: "23rem",
                width: "100vh",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                {newPlaybookParameters.length !== 0 ||
                generated.length !== 0 ? (
                  <>
                    <Box
                      sx={{
                        display: "flex",
                        flexDirection: "row",
                        paddingTop: isDesktop ? "3rem" : "10rem",
                      }}
                    >
                      <SwitchButton />
                      <PreviewWindowInfoPopover />
                    </Box>

                    {isYamlView && generated.length !== 0 ? (
                      <AceComponent value={generated} theme={theme} />
                    ) : (
                      <AceComponent value={aceCode} theme={theme} />
                    )}
                  </>
                ) : (
                  <Box>
                    <Illustrate />
                  </Box>
                )}
                <MessageSnackbar
                  open={message}
                  message={message}
                  onClose={handleSnackbarClose}
                  messageType={messageType}
                />
              </Box>
            </Box>
          </Box>
        </Box>
      </ImportedComponentBox>
    </MainBox>
  );
};

export default Keywords;
