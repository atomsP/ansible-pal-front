import React, { useState, useEffect, useContext } from "react";
import { List } from "react-virtualized";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useMediaQuery } from "react-responsive";

import { Button, Box, useTheme } from "@mui/material";
import Typography from "@mui/material/Typography";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import ArrowBackIosOutlinedIcon from "@mui/icons-material/ArrowBackIosOutlined";

import { AxiosContext } from "../../services/AxiosContext";
import { VisitedCollectionsContext } from "../../services/VisitedContext";

import {
  NewPlaybookParameterContext,
  GeneratedContext,
  TreeViewContext,
  YamlViewContext,
  EditPressedContext,
  AddPressedContext,
} from "../playbookBuilder/PlaybookBuilderContexts";

import Header from "../../components/Header";
import Body from "../../components/Body";
import PlaybookInstructions from "../global/instructions/PlaybookInstructions";
import img from "../../assets/ansiblePlaypompo.png";
import PreviewWindowInfoPopover from "../global/instructions/PreviewWindowInfoPopover";
import { translateToAceFormat } from "./functions/translateToAceFormat";
import { generatePlaybook } from "./functions/generatePlaybook";
import "../global/ace.css";
import LoadingAnimation from "../global/LoadingAnimation";

import MessageSnackbar from "../global/Snackbar";
import {
  MainBox,
  PacmanBox,
  NoConnectionBox,
  ConfirmationModal,
  AceComponent,
  PopoverTypography,
  PopoverComponent,
  HeadingBox,
  HeaderAndPopoverBox,
  ImportedComponentBox,
} from "../../components/components";
import imgError from "../../assets/noConnection.png";

const SelectCollections = () => {
  const [collections, setCollections] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const [anchorEl, setAnchorEl] = useState(null);
  const [loading, setLoading] = useState(true);
  const [generating, setGenerating] = useState(false);
  const [message, setMessage] = useState(null);
  const [messageType, setMessagetype] = useState(null);
  const [imgLoaded, setImgLoaded] = useState(false);
  const { generated, setGenerated } = useContext(GeneratedContext);
  const axiosContext = useContext(AxiosContext);
  const { isTreeView, setIsTreeView } = useContext(TreeViewContext);
  const { isYamlView, setIsYamlView } = useContext(YamlViewContext);
  const { newPlaybookParameters } = useContext(NewPlaybookParameterContext);
  const { isCollectionsVisited, setIsCollectionsVisited } = useContext(
    VisitedCollectionsContext
  );
  const { setIsAddPressed } = useContext(AddPressedContext);
  const { setIsEditPressed } = useContext(EditPressedContext);

  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const parentId = queryParams.get("parentId");
  const navigate = useNavigate();
  const theme = useTheme();

  const generatingOpen = () => setGenerating(true);
  const generatingClose = () => setGenerating(false);

  const isDesktop = useMediaQuery({ minWidth: 1128 });
  const isSmall = useMediaQuery({ maxWidth: 610 });
  const isMobile = useMediaQuery({ maxWidth: 767 });

  const handleFirstVisit = () => {
    setIsCollectionsVisited(true);
  };

  useEffect(() => {
    axiosContext.publicAxios
      .get("collections/", { timeout: 3500 })
      .then((response) => {
        setCollections(response.data);
        setTimeout(() => {
          setLoading(false);
          handleFirstVisit();
        }, 2500);
      })
      .catch((error) => {
        if (error.response) {
          const errorMessage = `Server responded with an error: ${error.response.data}`;
          setLoading(false);

          setMessage(errorMessage);
          setMessagetype("error");
        } else if (error.request) {
          const errorMessage = "No response received from the server";
          setLoading(false);

          setMessage(errorMessage);
          setMessagetype("error");
        } else {
          const errorMessage = `Error occurred while sending the request: ${error.message}`;
          setLoading(false);

          setMessage(errorMessage);
          setMessagetype("error");
        }
      });
  }, []);

  const handleSnackbarClose = () => {
    setMessage(null);
  };

  //---------------Popover

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  //-----------------------

  const handleSearchQueryChange = (event) => {
    setSearchQuery(event.target.value);
  };

  // const style = {
  //   position: "absolute",
  //   top: "50%",
  //   left: "50%",
  //   transform: "translate(-50%, -50%)",
  //   width: 400,
  //   bgcolor: theme.palette.modal.background,
  //   border: "2px solid #000",
  //   boxShadow: 24,
  //   p: 4,
  // };

  const rowRenderer = ({ index, style }) => {
    const collection = filteredCollections[index];
    const collectionData = {
      title: collection.title,
      id: collection.id,
    };
    const rowStyle = {
      ...style,
      color: "transparent",
      paddingLeft: "8rem",
    };
    return (
      <Link
        key={index}
        to={`/collections/${collection.id}?parentId=${parentId}&collectionTitle=${collection.title}&collectionId=${collection.id}`}
        style={rowStyle}
        state={collectionData}
      >
        <Body other={collection.title} />
      </Link>
    );
  };

  const filteredCollections = collections.filter((collection) =>
    collection.title.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const imgHeight = 250;

  const aceCode = translateToAceFormat(newPlaybookParameters);

  //----------------Components

  const GeneratePlaybookButton = () => {
    return (
      <Button
        sx={{
          m: 1.5,
          borderRadius: 0,
        }}
        variant="contained"
        color="info"
        onClick={() => {
          generatePlaybook({
            newPlaybookParameters,
            setGenerated,
            setGenerating,
            setMessage,
            setMessagetype,
          });
        }}
      >
        <span style={{ fontFamily: "Courier New", letterSpacing: "1px" }}>
          {isMobile ? "[Generate]" : "[Generate Playbook]"}
        </span>
      </Button>
    );
  };

  const ClearParametersButton = () => {
    return (
      <Button
        sx={{
          m: 1.5,
          borderRadius: 0,
          fontFamily: "Courier New",
          letterSpacing: "1px",
        }}
        variant="contained"
        color="secondary"
        onClick={() => {
          setIsAddPressed(false);
          setIsEditPressed(false);
          navigate(`/playbook`);
        }}
      >
        {isMobile ? "[Back]" : "[Back to Workshop]"}
      </Button>
    );
  };
  const SwitchButton = () => {
    return (
      <Button
        disabled={generated.length === 0}
        sx={{ m: 1.5 }}
        color="info"
        size="small"
        onClick={() => {
          setIsTreeView(!isTreeView);
          setIsYamlView(!isYamlView);
        }}
      >
        Tree/yAML
      </Button>
    );
  };
  const Illustrate = () => {
    return (
      <img
        alt="ansible-flag"
        style={{ height: imgHeight, width: imgHeight * 1.62 }}
        src={img}
      />
    );
  };
  const IllustrateError = () => {
    return (
      <img
        alt="no-connection"
        style={{
          height: imgHeight,
          width: imgHeight * 1.62,
          visibility: imgLoaded ? "visible" : "hidden",
        }}
        src={imgError}
        onLoad={() => setImgLoaded(true)}
      />
    );
  };

  return (
    <>
      {loading && !isCollectionsVisited ? (
        <PacmanBox>
          <LoadingAnimation />
        </PacmanBox>
      ) : collections.length === 0 && !isCollectionsVisited ? (
        <NoConnectionBox>
          {!imgLoaded && <div style={{ height: imgHeight }} />}
          <IllustrateError />
          <MessageSnackbar
            open={message}
            message={message}
            onClose={handleSnackbarClose}
            messageType={messageType}
          />
        </NoConnectionBox>
      ) : (
        <MainBox>
          <HeadingBox>
            <Box
              p={2}
              sx={{
                display: "flex",
                flexDirection: "row",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: !isDesktop ? "15rem" : "16rem",
                }}
              >
                <HeaderAndPopoverBox>
                  {isSmall ? (
                    <Header subtitle={`Collections`} />
                  ) : (
                    <Header subtitle={`Select Ansible collection`} />
                  )}
                  <PopoverTypography
                    open={open}
                    handlePopoverOpen={handlePopoverOpen}
                    handlePopoverClose={handlePopoverClose}
                    icon={<InfoOutlinedIcon />}
                    theme={theme}
                  />
                  <PopoverComponent
                    open={open}
                    anchorEl={anchorEl}
                    handlePopoverClose={handlePopoverClose}
                    text={<PlaybookInstructions />}
                  />
                </HeaderAndPopoverBox>
                <Typography>
                  <ArrowBackIosOutlinedIcon
                    style={{ color: "#placeholder", visibility: "hidden" }}
                  />
                </Typography>
              </Box>
              <Box>
                <Body title="Select Collection" />
                <input
                  type="text"
                  value={searchQuery}
                  onChange={handleSearchQueryChange}
                  placeholder="Search"
                />
              </Box>
            </Box>
          </HeadingBox>
          <ImportedComponentBox>
            <Box
              sx={{
                display: "flex",
                flexDirection: !isDesktop ? "column" : "row",
                alignItems: !isDesktop ? "center" : undefined,
                justifyContent: !isDesktop ? "center" : undefined,
                width: "100%",
                height: "100%",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: isSmall ? "50%" : "100%",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                    width: "100%",
                    height: "100%",
                  }}
                >
                  <List
                    width={isSmall ? 400 : 450}
                    height={400}
                    rowCount={filteredCollections.length}
                    rowHeight={40}
                    rowRenderer={rowRenderer}
                  />
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      paddingTop: !isDesktop ? "20px" : undefined,
                      paddingBottom: !isDesktop ? "10px" : undefined,
                    }}
                  >
                    {newPlaybookParameters &&
                    Object.keys(newPlaybookParameters).length !== 0 ? (
                      <>
                        <GeneratePlaybookButton />
                        <ConfirmationModal
                          open={generating}
                          onClose={generatingClose}
                          theme={theme}
                          generated={generated}
                        />
                        <ClearParametersButton />
                      </>
                    ) : null}
                  </Box>
                </Box>
              </Box>
              <Box>
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    height: "23rem",
                    width: "100vh",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    {newPlaybookParameters.length !== 0 ||
                    generated.length !== 0 ? (
                      <>
                        <Box
                          sx={{
                            display: "flex",
                            flexDirection: "row",
                            paddingTop: isDesktop ? "3rem" : "10rem",
                          }}
                        >
                          <SwitchButton />
                          <PreviewWindowInfoPopover />
                        </Box>
                        {isYamlView && generated.length !== 0 ? (
                          <AceComponent value={generated} theme={theme} />
                        ) : (
                          <AceComponent value={aceCode} theme={theme} />
                        )}
                      </>
                    ) : (
                      <Box>
                        <Illustrate />
                      </Box>
                    )}
                    <MessageSnackbar
                      open={message}
                      message={message}
                      onClose={handleSnackbarClose}
                      messageType={messageType}
                    />
                  </Box>
                </Box>
              </Box>
            </Box>
          </ImportedComponentBox>
        </MainBox>
      )}
    </>
  );
};

export default SelectCollections;
