import React, { useState, useEffect, useContext } from "react";
import { useParams, Link, useNavigate, useLocation } from "react-router-dom";
import { List } from "react-virtualized";
import { useMediaQuery } from "react-responsive";

import { Box, Button, useTheme } from "@mui/material";
import Typography from "@mui/material/Typography";
import ArrowBackIosOutlinedIcon from "@mui/icons-material/ArrowBackIosOutlined";
import ArrowForwardIosOutlinedIcon from "@mui/icons-material/ArrowForwardIosOutlined";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";

import { AxiosContext } from "../../services/AxiosContext";

import {
  NewPlaybookParameterContext,
  GeneratedContext,
  TreeViewContext,
  YamlViewContext,
  EditPressedContext,
  AddPressedContext,
} from "../playbookBuilder/PlaybookBuilderContexts";

import img from "../../assets/ansiblePlaypompo.png";
import Header from "../../components/Header";
import Body from "../../components/Body";
import PreviewWindowInfoPopover from "../global/instructions/PreviewWindowInfoPopover";
import { translateToAceFormat } from "./functions/translateToAceFormat";
import { generatePlaybook } from "./functions/generatePlaybook";
import "../global/ace.css";
import MessageSnackbar from "../global/Snackbar";

import {
  MainBox,
  ConfirmationModal,
  AceComponent,
  pulsingAnimation,
  PopoverTypography,
  PopoverComponent,
  HeadingBox,
  HeaderAndPopoverBox,
  ImportedComponentBox,
} from "../../components/components";

const Modules = () => {
  const [modules, setModules] = useState([]);
  const { generated, setGenerated } = useContext(GeneratedContext);
  const [generating, setGenerating] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [popoverModuleComments, setPopoverModuleComments] = useState("");
  const [selectedModules, setSelectedModules] = useState([]);
  const [editMode] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");
  const [filteredModules, setFilteredModules] = useState([]);
  const [message, setMessage] = useState(null);
  const [messageType, setMessagetype] = useState(null);

  const axiosContext = useContext(AxiosContext);
  const { isTreeView, setIsTreeView } = useContext(TreeViewContext);
  const { isYamlView, setIsYamlView } = useContext(YamlViewContext);
  const { newPlaybookParameters } = useContext(NewPlaybookParameterContext);

  const { setIsAddPressed } = useContext(AddPressedContext);
  const { setIsEditPressed } = useContext(EditPressedContext);

  const generatingOpen = () => setGenerating(true);
  const generatingClose = () => setGenerating(false);

  const theme = useTheme();

  const isDesktop = useMediaQuery({ minWidth: 1128 });
  const isSmall = useMediaQuery({ maxWidth: 610 });
  const isMobile = useMediaQuery({ maxWidth: 767 });

  const { id } = useParams();
  const navigate = useNavigate();
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const parentId = queryParams.get("parentId");
  const collectionTitle = queryParams.get("collectionTitle");
  const collectionId = queryParams.get("collectionId");

  useEffect(() => {
    axiosContext.publicAxios
      .get(`collection_modules/${id}/`, { timeout: 3500 })
      .then((response) => {
        setModules(response.data);
      })
      .catch((error) => {
        if (error.response) {
          const errorMessage = `Server responded with an error: ${error.response.data}`;
          setMessage(errorMessage);
          setMessagetype("error");
        } else if (error.request) {
          const errorMessage = "No response received from the server";
          setMessage(errorMessage);
          setMessagetype("error");
        } else {
          const errorMessage = `Error occurred while sending the request: ${error.message}`;
          setMessage(errorMessage);
          setMessagetype("error");
        }
      });
  }, [id]);

  const handleSnackbarClose = () => {
    setMessage(null);
  };

  //--------------Popover

  const handlePopoverOpen = (event, moduleComments) => {
    setPopoverModuleComments(moduleComments);
    setAnchorEl(event.currentTarget);
  };
  const handlePopoverClose = () => {
    setAnchorEl(null);
  };
  const popOpen = Boolean(anchorEl);

  const handleCheckboxChange = (event) => {
    const moduleId = parseInt(event.target.value);
    const moduleTitle = event.target.getAttribute("data-title");
    if (event.target.checked) {
      setSelectedModules([
        ...selectedModules,
        { id: moduleId, title: moduleTitle },
      ]);
    } else {
      setSelectedModules(
        selectedModules.filter((module) => module.id !== moduleId)
      );
    }
  };

  const getModuleData = async (moduleId) => {
    const response = await axiosContext.publicAxios.get(`modules/${moduleId}/`);
    return response.data;
  };

  const handleEditSubmit = async () => {
    const moduleDataRequests = selectedModules.map((module) =>
      getModuleData(module.id)
    );
    const moduleDataResponses = await Promise.all(moduleDataRequests);
    const title = modules[0]?.collection;
    navigate(
      `/collections/${id}/selectedModules?parentId=${parentId}&collectionTitle=${collectionTitle}&collectionId=${collectionId}`,
      {
        state: { moduleDataResponses, title },
      }
    );
  };

  useEffect(() => {
    const filtered = modules.filter((module) => {
      return module.title.toLowerCase().includes(searchTerm.toLowerCase());
    });
    setFilteredModules(filtered);
  }, [modules, searchTerm]);

  const rowRenderer = ({ index, style }) => {
    const module = filteredModules[index];
    const moduleComments = module.description;
    const rowStyle = {
      ...style,
      color: "transparent",
      paddingLeft: "8rem",
    };
    return (
      <div key={index} style={rowStyle}>
        <Box display="flex">
          <Box sx={{ marginRight: 1 }}>
            <PopoverTypography
              open={popOpen}
              handlePopoverOpen={(event) =>
                handlePopoverOpen(event, moduleComments)
              }
              handlePopoverClose={handlePopoverClose}
              icon={<InfoOutlinedIcon />}
              theme={theme}
            />
            <PopoverComponent
              open={popOpen}
              anchorEl={anchorEl}
              handlePopoverClose={handlePopoverClose}
              text={
                <Box sx={{ padding: 2, maxWidth: 300 }}>
                  {popoverModuleComments}
                </Box>
              }
            />
          </Box>
          <input
            type="checkbox"
            value={module.id}
            data-title={module.title}
            onChange={handleCheckboxChange}
            checked={selectedModules.some((m) => m.id === module.id)}
          />
          <Body other={module.title} />
        </Box>
      </div>
    );
  };

  const imgHeight = 250;

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: theme.palette.modal.background,
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  const aceCode = translateToAceFormat(newPlaybookParameters);

  //----------------Components

  const GeneratePlaybookButton = () => {
    return (
      <Button
        sx={{
          m: 1.5,
          borderRadius: 0,
        }}
        variant="contained"
        color="info"
        onClick={() => {
          generatePlaybook({
            newPlaybookParameters,
            setGenerated,
            setGenerating,
            setMessage,
            setMessagetype,
          });
        }}
      >
        <span style={{ fontFamily: "Courier New", letterSpacing: "1px" }}>
          {isMobile ? "[Generate]" : "[Generate Playbook]"}
        </span>
      </Button>
    );
  };

  const ClearParametersButton = () => {
    return (
      <Button
        sx={{
          m: 1.5,
          borderRadius: 0,
          fontFamily: "Courier New",
          letterSpacing: "1px",
        }}
        variant="contained"
        color="secondary"
        onClick={() => {
          setIsAddPressed(false);
          setIsEditPressed(false);
          navigate(`/playbook`);
        }}
      >
        {isMobile ? "[Back]" : "[Back to Workshop]"}
      </Button>
    );
  };
  const SwitchButton = () => {
    return (
      <Button
        disabled={generated.length === 0}
        sx={{ m: 1.5 }}
        color="info"
        size="small"
        onClick={() => {
          setIsTreeView(!isTreeView);
          setIsYamlView(!isYamlView);
        }}
      >
        Tree/yAML
      </Button>
    );
  };
  const Illustrate = () => {
    return (
      <img
        alt="ansible-flag"
        style={{ height: imgHeight, width: imgHeight * 1.62 }}
        src={img}
      />
    );
  };

  return (
    <MainBox>
      <HeadingBox>
        <Box
          p={2}
          sx={{
            display: "flex",
            flexDirection: "row",
          }}
        >
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              width: !isDesktop ? "15rem" : "16rem",
            }}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
              }}
            >
              <HeaderAndPopoverBox>
                <Header subtitle={modules[0]?.collection} />
              </HeaderAndPopoverBox>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                }}
              >
                <Typography>
                  <Link to={`/selectCollections?parentId=${parentId}`}>
                    <ArrowBackIosOutlinedIcon
                      style={{
                        color: theme.palette.list.arrowBack,
                      }}
                    />
                  </Link>
                </Typography>
                {!editMode && selectedModules.length > 0 ? (
                  <>
                    <Typography
                      sx={{
                        animation: `${`${pulsingAnimation(1.25)} 2s infinite`}`,
                      }}
                    >
                      <Link onClick={handleEditSubmit}>
                        <ArrowForwardIosOutlinedIcon
                          style={{
                            color: theme.palette.list.arrowForward,
                          }}
                        />
                      </Link>
                    </Typography>
                  </>
                ) : null}
              </Box>
            </Box>
          </Box>

          <Box>
            <Body title="Select Modules" />
            <input
              type="text"
              placeholder="Search"
              value={searchTerm}
              onChange={(e) => setSearchTerm(e.target.value)}
            />
          </Box>
        </Box>
      </HeadingBox>
      <ImportedComponentBox>
        <Box
          sx={{
            display: "flex",
            flexDirection: !isDesktop ? "column" : "row",
            alignItems: !isDesktop ? "center" : undefined,
            justifyContent: !isDesktop ? "center" : undefined,
            width: "100%",
            height: "100%",
          }}
        >
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              width: isSmall ? "50%" : "100%",
            }}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
                height: "100%",
              }}
            >
              <List
                width={isSmall ? 400 : 450}
                height={400}
                rowCount={filteredModules.length}
                rowHeight={40}
                rowRenderer={rowRenderer}
              />
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  paddingTop: !isDesktop ? "20px" : undefined,
                  paddingBottom: !isDesktop ? "10px" : undefined,
                }}
              >
                {newPlaybookParameters &&
                Object.keys(newPlaybookParameters).length !== 0 ? (
                  <>
                    <GeneratePlaybookButton />
                    <ConfirmationModal
                      open={generating}
                      onClose={generatingClose}
                      theme={theme}
                      generated={generated}
                    />
                    <ClearParametersButton />
                  </>
                ) : null}
              </Box>
            </Box>
          </Box>
          <Box>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                height: "23rem",
                width: "100vh",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                {newPlaybookParameters.length !== 0 ||
                generated.length !== 0 ? (
                  <>
                    <Box
                      sx={{
                        display: "flex",
                        flexDirection: "row",
                        paddingTop: isDesktop ? "3rem" : "10rem",
                      }}
                    >
                      <SwitchButton />
                      <PreviewWindowInfoPopover />
                    </Box>
                    {isYamlView && generated.length !== 0 ? (
                      <AceComponent value={generated} theme={theme} />
                    ) : (
                      <AceComponent value={aceCode} theme={theme} />
                    )}
                  </>
                ) : (
                  <Box>
                    <Illustrate />
                  </Box>
                )}
                <MessageSnackbar
                  open={message}
                  message={message}
                  onClose={handleSnackbarClose}
                  messageType={messageType}
                />
              </Box>
            </Box>
          </Box>
        </Box>
      </ImportedComponentBox>
    </MainBox>
  );
};

export default Modules;
