import React, { useState, useContext, useEffect } from "react";
import { useTheme } from "@mui/material";
import Modal from "@mui/material/Modal";

import {
  NewPlaybookParameterContext,
  ShowButtonContext,
  AttributesContext,
  AttributesPresentContext,
  AddKeywordsPressedContext,
  AddModulesPressedContext,
  SelectedNodeIDContext,
  SelectedNodeNameContext,
  SelectedNodeTypeContext,
  EditPressedContext,
  AddPressedContext,
} from "../playbookBuilder/PlaybookBuilderContexts";

import { findNode } from "./functions/findNode";
import { RenderModalContent } from "./RenderModalContent";
import { ConfirmationDialog } from "../../components/components";

const RenderSvgNode = ({ nodeDatum }) => {
  const { showButton, setShowButton } = useContext(ShowButtonContext);
  const [node, setNode] = useState(null);
  const [nodeId, setNodeId] = useState(null);
  const [isNodeConfirmationOpen, setIsNodeConfirmationOpen] = useState(false);
  const { newPlaybookParameters, setNewPlaybookParameters } = useContext(
    NewPlaybookParameterContext
  );

  const [componentReady, setComponentReady] = useState(false);
  const { setAttributes } = useContext(AttributesContext);
  const { isAttributesPresent, setIsAttributesPresent } = useContext(
    AttributesPresentContext
  );
  const { isEditPressed, setIsEditPressed } = useContext(EditPressedContext);
  const { isAddPressed, setIsAddPressed } = useContext(AddPressedContext);
  const [isAddRolePressed, setIsAddRolePressed] = useState(false);
  const [insertRoleName, setInsertRoleName] = useState("");
  const [selectedType, setSelectedType] = useState("");
  const { isAddKeywordsPressed, setIsAddKeywordsPressed } = useContext(
    AddKeywordsPressedContext
  );
  const { isAddModulesPressed, setIsAddModulesPressed } = useContext(
    AddModulesPressedContext
  );
  const [isInsertRolePressed, setIsInsertRolePressed] = useState(false);
  const [isEditNamePressed, setIsEditNamePressed] = useState(false);
  const [isEditHostsPressed, setIsEditHostsPressed] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [editedNodeName, setEditedNodeName] = useState("");
  const [hostsName, setHostsName] = useState("all");
  const { selectedNodeId, setSelectedNodeId } = useContext(
    SelectedNodeIDContext
  );
  const { setSelectedNodeName } = useContext(SelectedNodeNameContext);
  const { setSelectedNodeType } = useContext(SelectedNodeTypeContext);

  const theme = useTheme();

  useEffect(() => {
    setComponentReady(true);
  }, []);

  const handleEditName = (nodeId) => {
    setNewPlaybookParameters((prevParameters) => {
      const updatedParameters = [...prevParameters];
      const node = findNode(updatedParameters, nodeId);

      if (node) {
        node.name = editedNodeName;
      }

      return updatedParameters;
    });
  };
  const handleEditHosts = (nodeId) => {
    setNewPlaybookParameters((prevParameters) => {
      const updatedParameters = [...prevParameters];
      const node = findNode(updatedParameters, nodeId);
      if (node) {
        node.hosts = hostsName;
      }
      return updatedParameters;
    });
  };

  const handleMouseOver = (nodeId, nodeName, nodeType) => {
    setSelectedNodeId(nodeId);
    setSelectedNodeName(nodeName);
    if (nodeType === undefined) {
      setSelectedNodeType("Play");
    } else {
      setSelectedNodeType(nodeType);
    }
    setShowButton((prev) => {
      if (isAddPressed || isEditPressed) {
        return prev;
      }
      const updatedShowButton = Object.keys(prev).reduce((acc, key) => {
        return {
          ...acc,
          [key]: false,
        };
      }, {});
      updatedShowButton[nodeId] = true;
      setIsAddPressed(false);
      setIsEditPressed(false);
      setEditedNodeName(nodeName);
      return updatedShowButton;
    });
  };

  const getRandomId = () => {
    return Math.random().toString(36).substring(2, 9);
  };

  const handleAddParam = (nodeId) => {
    const newNode = {
      name: "",
      type: selectedType,
      id: getRandomId(),
      attributes: {},
      children: [],
    };
    setNewPlaybookParameters((prevParameters) => {
      const updatedParameters = [...prevParameters];
      const parentNode = findNode(updatedParameters, nodeId);
      if (parentNode) {
        parentNode.children.push(newNode);
      }
      return updatedParameters;
    });
  };

  const handleTypeChange = (event) => {
    setSelectedType(event.target.value);
  };

  const handleRoleUpdate = (nodeId, selectedType) => {
    setNewPlaybookParameters((prevParameters) => {
      const updatedParameters = [...prevParameters];
      const node = findNode(updatedParameters, nodeId);

      if (node) {
        if (!node.roles) {
          node.roles = [];
        }

        const existingRole = node.roles.find(
          (role) => role.roleType === selectedType
        );

        if (existingRole) {
          existingRole.roleName = insertRoleName;
        } else {
          const newRole = { roleType: selectedType, roleName: insertRoleName };
          node.roles = [...node.roles, newRole];
        }
      }

      return updatedParameters;
    });
  };

  const findParentNode = (nodes, nodeId) => {
    for (let node of nodes) {
      if (node.children) {
        if (node.children.some((child) => child.id === nodeId)) {
          return node;
        }
        const parentNode = findParentNode(node.children, nodeId);
        if (parentNode) {
          return parentNode;
        }
      }
    }
    return null;
  };

  const handleRemoveNode = (nodeId) => {
    setNewPlaybookParameters((prevParameters) => {
      const updatedParameters = [...prevParameters];
      const parentNode = findParentNode(updatedParameters, nodeId);
      if (parentNode) {
        parentNode.children = parentNode.children.filter(
          (child) => child.id !== nodeId
        );
      } else {
        updatedParameters.splice(
          updatedParameters.findIndex((node) => node.id === nodeId)
        );
      }
      return updatedParameters;
    });
    setShowButton((prev) => {
      const updatedShowButton = { ...prev };
      delete updatedShowButton[nodeId];
      return updatedShowButton;
    });
  };

  const handleNodeClearClick = () => {
    setIsNodeConfirmationOpen(true);
  };

  const handleConfirmNodeClear = (nodeId) => {
    setIsNodeConfirmationOpen(false);
    handleRemoveNode(nodeId);
  };

  const handleCloseNodeConfirmation = () => {
    setIsNodeConfirmationOpen(false);
  };

  const handleEditButtonClick = (nodeId) => {
    const node = findNode(newPlaybookParameters, nodeId);
    if (node) {
      setNode(node);
      setNodeId(node.id);
      setAttributes(node.attributes);
      if (
        (node.id === "Play" &&
          Object.keys(node.attributes.keywords).length === 0) ||
        ((!node.attributes.parameters ||
          node.attributes.parameters.length === 0) &&
          (!node.attributes.keywords ||
            Object.keys(node.attributes.keywords).length === 0) &&
          (!node.roles || node.roles.length === 0))
      ) {
        setIsAttributesPresent(false);
      } else {
        setIsAttributesPresent(true);
      }
    }
    setIsEditPressed(!isEditPressed);
    setIsAddPressed(false);
    setIsAddRolePressed(false);
  };

  const handleAddChildButtonClick = () => {
    setIsAddPressed(!isAddPressed);
    setIsEditPressed(false);
  };
  const handleAddRoleButtonClick = () => {
    setIsAddRolePressed(!isAddRolePressed);
    setIsEditPressed(false);
  };
  const handleSelectRoleConfirmClick = () => {
    setIsAddPressed(false);
    setIsEditPressed(false);
    setIsAddRolePressed(false);
    setInsertRoleName("");
  };
  const handleSelectRoleCancelClick = () => {
    setSelectedType("");
    setIsAddPressed(false);
    setIsEditPressed(false);
    setIsAddRolePressed(false);
  };
  const handlAddKeywordsButtonClick = () => {
    setIsAddKeywordsPressed(!isAddKeywordsPressed);
  };
  const handleInsertRoleButtonClick = () => {
    setIsInsertRolePressed(!isInsertRolePressed);
  };
  const handleInsertRoleConfirmClick = () => {
    setIsAddPressed(false);
    setIsEditPressed(false);
    setIsInsertRolePressed(false);
    // setInsertRoleName("");
  };
  const handleInsertRoleCancelClick = () => {
    setIsAddPressed(false);
    setIsEditPressed(false);
    setIsInsertRolePressed(false);
  };
  const handleAddModulesButtonClick = () => {
    setIsAddModulesPressed(!isAddModulesPressed);
  };
  const handleEditNameButtonClick = () => {
    setIsEditNamePressed(true);
  };
  const handleEditHostsButtonClick = () => {
    setIsEditHostsPressed(true);
  };
  const handleEditNameConfirmClick = () => {
    setIsAddPressed(false);
    setIsEditPressed(false);
    setIsEditNamePressed(false);
  };
  const handleEditHostsConfirmClick = () => {
    setIsAddPressed(false);
    setIsEditPressed(false);
    setIsEditHostsPressed(false);
  };
  const handleEditNameCancelClick = () => {
    setIsAddPressed(false);
    setIsEditPressed(false);
    setIsEditNamePressed(false);
  };
  const handleEditHostsCancelClick = () => {
    setIsAddPressed(false);
    setIsEditPressed(false);
    setIsEditHostsPressed(false);
  };
  const handleEditAttributesButtonClick = () => {
    setIsOpen(true);
  };
  const handleRoleTypeInput = (value) => {
    setInsertRoleName(value);
  };
  const handleSelectNodeTypeConfirm = (nodeId) => {
    if (selectedType) {
      handleAddParam(nodeId);
      setIsAddPressed(false);
      setShowButton((prev) => ({
        ...prev,
        [nodeId]: false,
      }));
      setSelectedType("");
    }
  };
  const handleSelectNodeTypeCancel = () => {
    setSelectedType("");
    setIsAddPressed(false);
  };
  const handleEditNodeName = (value) => {
    setEditedNodeName(value);
  };
  const handleEditHostsName = (value) => {
    setHostsName(value);
  };
  const handleInsertRoleName = (value) => {
    setInsertRoleName(value);
  };
  const handleTreeModalClose = (value) => {
    setIsOpen(value);
  };

  const InputBundle = ({
    value,
    onChange,
    selectedNodeId,
    selectedType,
    handleConfirmAction,
    handleConfirmClick,
    handleCancelClick,
  }) => {
    const handleKeyDown = (event) => {
      if (event.key === "Enter") {
        event.preventDefault();
        document.getElementById("confirmButton").click();
      }
    };

    const handleConfirm = (e) => {
      e.stopPropagation();
      if (selectedNodeId && selectedType) {
        handleConfirmAction(selectedNodeId, selectedType);
      } else if (selectedNodeId) {
        handleConfirmAction(selectedNodeId);
      }
      handleConfirmClick();
    };

    const handleCancel = (e) => {
      e.stopPropagation();
      handleCancelClick();
    };

    return (
      <>
        <input
          type="text"
          autoFocus
          value={value}
          onKeyDown={handleKeyDown}
          onChange={(event) => onChange(event.target.value)}
          style={{ width: "100%" }}
        />
        <button id="confirmButton" onClick={handleConfirm}>
          Confirm
        </button>
        <button onClick={handleCancel}>X</button>
      </>
    );
  };

  return (
    componentReady && (
      <g>
        <g>
          {nodeDatum.type === "Block" ? (
            <rect
              width="30"
              height="25"
              x="-15"
              y="-10"
              fill={theme.palette.builderNodes.block}
              onClick={(e) => {
                e.stopPropagation();
              }}
              onMouseOver={() => {
                handleMouseOver(nodeDatum.id, nodeDatum.name, nodeDatum.type);
              }}
            />
          ) : (
            <circle
              cx="0"
              cy="2"
              r={nodeDatum.id === "Play" ? "15" : "14"}
              fill={
                nodeDatum.id === "Play"
                  ? theme.palette.builderNodes.mainCircle
                  : nodeDatum.type === "Role"
                  ? theme.palette.builderNodes.role
                  : theme.palette.builderNodes.circle
              }
              onClick={(e) => {
                e.stopPropagation();
              }}
              onMouseOver={() => {
                handleMouseOver(nodeDatum.id, nodeDatum.name, nodeDatum.type);
              }}
            />
          )}
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}

          <a href="#" onClick={(e) => e.stopPropagation()}>
            <text
              fill={theme.palette.builderNodes.nodeName}
              strokeWidth="0"
              x="20"
              y="10"
            >
              {nodeDatum.name !== undefined
                ? nodeDatum.name.substring(0, 8) +
                  (nodeDatum.name.length > 8 ? "..." : "") +
                  " " +
                  (nodeDatum.id === "Play" ? nodeDatum.id : nodeDatum.type)
                : nodeDatum.id === "Play"
                ? nodeDatum.id
                : nodeDatum.type}
            </text>
          </a>
        </g>
        {showButton[nodeDatum.id] && (
          <g>
            {nodeDatum.type !== "Task" &&
              nodeDatum.type !== "Role" &&
              nodeDatum.type !== "include_role" &&
              nodeDatum.type !== "import_role" && (
                <text
                  fill={
                    isAddPressed
                      ? theme.palette.builderNodes.leftButtonPressedFill
                      : theme.palette.builderNodes.buttonColorFill
                  }
                  stroke={
                    isAddPressed
                      ? theme.palette.builderNodes.leftButtonPressedStroke
                      : theme.palette.builderNodes.buttonColorStroke
                  }
                  x="-75"
                  y="-15"
                  onClick={(e) => {
                    e.stopPropagation();
                    handleAddChildButtonClick();
                  }}
                >
                  ADD CHILD
                  {isAddPressed && <set attributeName="font-size" to="15" />}
                </text>
              )}
            {nodeDatum.type === "Task" && (
              <text
                fill={
                  isAddRolePressed
                    ? theme.palette.builderNodes.leftButtonPressedFill
                    : theme.palette.builderNodes.buttonColorFill
                }
                stroke={
                  isAddRolePressed
                    ? theme.palette.builderNodes.leftButtonPressedStroke
                    : theme.palette.builderNodes.buttonColorStroke
                }
                x="-75"
                y="-15"
                onClick={(e) => {
                  e.stopPropagation();
                  handleAddRoleButtonClick();
                }}
              >
                ADD ROLE
                {isAddRolePressed && <set attributeName="font-size" to="15" />}
              </text>
            )}
            {isAddRolePressed && nodeDatum.type === "Task" && (
              <foreignObject x="25" y="-25" width="100" height="100">
                <select
                  value={selectedType}
                  onChange={(e) => {
                    e.stopPropagation();
                    handleTypeChange(e);
                  }}
                >
                  <option value="">Select Role</option>
                  <option value="include_role">include_role</option>
                  <option value="import_role">import_role</option>
                </select>

                {selectedType && (
                  <InputBundle
                    value={insertRoleName}
                    onChange={(value) => handleRoleTypeInput(value)}
                    selectedNodeId={selectedNodeId}
                    selectedType={selectedType}
                    handleConfirmAction={handleRoleUpdate}
                    handleConfirmClick={handleSelectRoleConfirmClick}
                    handleCancelClick={handleSelectRoleCancelClick}
                  />
                )}
              </foreignObject>
            )}
            {isAddPressed && nodeDatum.type !== "Task" && (
              <foreignObject x="25" y="-25" width="100" height="100">
                <select
                  value={selectedType}
                  onChange={(e) => {
                    e.stopPropagation();
                    handleTypeChange(e);
                  }}
                >
                  <option value="">Select Type</option>
                  <option value="Block">Block</option>
                  <option value="Task">Task</option>
                  <option value="Role">Role</option>
                </select>
                {selectedType && (
                  <>
                    <button
                      onClick={(e) => {
                        e.stopPropagation();
                        handleSelectNodeTypeConfirm(nodeDatum.id);
                      }}
                    >
                      Confirm
                    </button>
                    <button
                      onClick={(e) => {
                        e.stopPropagation();
                        handleSelectNodeTypeCancel();
                      }}
                    >
                      X
                    </button>
                  </>
                )}
              </foreignObject>
            )}
            <text
              fill={
                isEditPressed
                  ? theme.palette.builderNodes.leftButtonPressedFill
                  : theme.palette.builderNodes.buttonColorFill
              }
              stroke={
                isEditPressed
                  ? theme.palette.builderNodes.leftButtonPressedStroke
                  : theme.palette.builderNodes.buttonColorStroke
              }
              x="-75"
              y={nodeDatum.id === "Play" ? 5 : 25}
              onClick={(e) => {
                e.stopPropagation();
                handleEditButtonClick(nodeDatum.id);
              }}
            >
              EDIT
              {isEditPressed && <set attributeName="font-size" to="15" />}
            </text>
            {isEditPressed && (
              <>
                {isAttributesPresent && (
                  <text
                    fill={theme.palette.builderNodes.buttonColorFill}
                    stroke={theme.palette.builderNodes.buttonColorStroke}
                    x="25"
                    y="-35"
                    onClick={(e) => {
                      e.stopPropagation();
                      handleEditAttributesButtonClick();
                    }}
                  >
                    EDIT ATTRIBUTES
                  </text>
                )}
                <Modal
                  open={isOpen}
                  onClose={() => handleTreeModalClose(false)}
                >
                  <RenderModalContent
                    node={node}
                    nodeId={nodeId}
                    isAttributesPresent={isAttributesPresent}
                  />
                </Modal>

                <text
                  fill={theme.palette.builderNodes.buttonColorFill}
                  stroke={theme.palette.builderNodes.buttonColorStroke}
                  x="25"
                  y={
                    nodeDatum.type === "Role"
                      ? 45
                      : nodeDatum.type === undefined
                      ? 45
                      : 65
                  }
                  onClick={(e) => {
                    e.stopPropagation();
                    handleEditNameButtonClick();
                  }}
                >
                  EDIT NAME
                </text>

                {nodeDatum.type === undefined && (
                  <text
                    fill={theme.palette.builderNodes.buttonColorFill}
                    stroke={theme.palette.builderNodes.buttonColorStroke}
                    x="25"
                    y="-15"
                    onClick={(e) => {
                      e.stopPropagation();
                      handleEditHostsButtonClick();
                    }}
                  >
                    EDIT HOSTS
                  </text>
                )}
                {isEditHostsPressed && (
                  <foreignObject x="20" y="-35" width="150" height="100">
                    <InputBundle
                      value={hostsName}
                      onChange={(value) => handleEditHostsName(value)}
                      selectedNodeId={selectedNodeId}
                      handleConfirmAction={handleEditHosts}
                      handleConfirmClick={handleEditHostsConfirmClick}
                      handleCancelClick={handleEditHostsCancelClick}
                    />
                  </foreignObject>
                )}

                {isEditNamePressed ? (
                  <foreignObject x="20" y="-35" width="150" height="100">
                    <InputBundle
                      value={editedNodeName}
                      onChange={(value) => handleEditNodeName(value)}
                      selectedNodeId={selectedNodeId}
                      handleConfirmAction={handleEditName}
                      handleConfirmClick={handleEditNameConfirmClick}
                      handleCancelClick={handleEditNameCancelClick}
                    />
                  </foreignObject>
                ) : (
                  <>
                    {nodeDatum.type !== "Role" &&
                      nodeDatum.type !== "include_role" &&
                      nodeDatum.type !== "import_role" && (
                        <text
                          fill={theme.palette.builderNodes.buttonColorFill}
                          stroke={theme.palette.builderNodes.buttonColorStroke}
                          x="25"
                          y="25"
                          // y={nodeDatum.id === "Play" ? -15 : 25}
                          onClick={(e) => {
                            e.stopPropagation();
                            handlAddKeywordsButtonClick();
                          }}
                        >
                          ADD KEYWORDS
                        </text>
                      )}

                    {nodeDatum.id !== "Play" &&
                      nodeDatum.type !== "Role" &&
                      nodeDatum.type !== "include_role" &&
                      nodeDatum.type !== "import_role" && (
                        <text
                          fill={theme.palette.builderNodes.buttonColorFill}
                          stroke={theme.palette.builderNodes.buttonColorStroke}
                          x="25"
                          y="45"
                          onClick={(e) => {
                            e.stopPropagation();
                            handleAddModulesButtonClick();
                          }}
                        >
                          ADD MODULES
                        </text>
                      )}
                    {nodeDatum.id !== "Play" &&
                      nodeDatum.type !== "Task" &&
                      nodeDatum.type !== "Block" && (
                        <text
                          fill={theme.palette.builderNodes.buttonColorFill}
                          stroke={theme.palette.builderNodes.buttonColorStroke}
                          x="25"
                          y={
                            nodeDatum.id === "Play"
                              ? 25
                              : 35 || nodeDatum.id === "Role"
                              ? 25
                              : 35
                          }
                          onClick={(e) => {
                            e.stopPropagation();
                            handleInsertRoleButtonClick();
                          }}
                        >
                          INSERT ROLE
                        </text>
                      )}
                    {isInsertRolePressed && (
                      <foreignObject x="20" y="-35" width="150" height="100">
                        <InputBundle
                          value={insertRoleName}
                          onChange={(value) => handleInsertRoleName(value)}
                          selectedNodeId={selectedNodeId}
                          handleConfirmAction={handleRoleUpdate}
                          handleConfirmClick={handleInsertRoleConfirmClick}
                          handleCancelClick={handleInsertRoleCancelClick}
                        />
                      </foreignObject>
                    )}
                  </>
                )}
              </>
            )}
            {nodeDatum.id !== "Play" && (
              <>
                <text
                  fill={theme.palette.builderNodes.buttonColorFill}
                  stroke={theme.palette.builderNodes.buttonColorStroke}
                  x="-75"
                  y="45"
                  onClick={(e) => {
                    e.stopPropagation();
                    if (!isEditPressed && !isAddPressed && !isAddRolePressed) {
                      handleNodeClearClick();
                    }
                  }}
                  style={{
                    opacity:
                      isEditPressed || isAddPressed || isAddRolePressed
                        ? 0.5
                        : 1,
                  }}
                >
                  REMOVE
                </text>
                <ConfirmationDialog
                  theme={theme}
                  open={isNodeConfirmationOpen}
                  onClose={() => {
                    handleCloseNodeConfirmation();
                  }}
                  title="Confirm"
                  content={
                    nodeDatum.children.length !== 0
                      ? "Whole node will be gone and children too"
                      : "Whole node will be gone"
                  }
                  onClickNo={() => {
                    handleCloseNodeConfirmation();
                  }}
                  onClickYes={() => {
                    handleConfirmNodeClear(nodeDatum.id);
                  }}
                />
              </>
            )}
          </g>
        )}
      </g>
    )
  );
};

export default RenderSvgNode;
