import * as React from "react";
import Typography from "@mui/material/Typography";

const Instructions = () => {
  return (
    <Typography sx={{ p: 1 }}>
      Add group name, then hostnames. Press ADD then GENERATE
    </Typography>
  );
};

export default Instructions;
