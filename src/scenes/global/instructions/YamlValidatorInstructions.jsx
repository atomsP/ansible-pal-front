import * as React from "react";
import Typography from "@mui/material/Typography";

const Instructions = () => {
  return (
    <Typography sx={{ p: 1 }}>
      1. Upload or paste your YAML file into the Validator
      <br /> 2. If the line is marked with the red cross (on the left side),
      hover on it and read through information to find out about the error.
      Please note that Validator marks the first error in the file, once you
      take care of it it will show any subsequent errors, if present.
      <br />
      3. Look for errors on the line with the cross, or the lines above and
      below it as sometimes the error can be a result of something a few lines
      earlier in the file{" "}
    </Typography>
  );
};

export default Instructions;
