import * as React from "react";
import Typography from "@mui/material/Typography";

const KeywordsInstructions = () => {
  return (
    <Typography sx={{ p: 1 }}>
      1. Select keyword from the list
      <br /> 2. Add value
      <br />
      3. Once you are finished press "Generate Playbook". Your playbook will be
      displayed in text editor window{" "}
    </Typography>
  );
};

export default KeywordsInstructions;
