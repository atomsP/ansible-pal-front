import * as React from "react";
import Typography from "@mui/material/Typography";

const PlaybookInstructions = () => {
  return (
    <Typography sx={{ p: 1 }}>
      1. Select collection from the list
      <br /> 2. Select modules/plugins, fill in parameter details. Selected
      details will be displayed in side window.
      <br />
      3. Once you are finished with tasks press "Generate Playbook". Your
      playbook will be displayed in text editor window{" "}
    </Typography>
  );
};

export default PlaybookInstructions;
