import * as React from "react";
import Typography from "@mui/material/Typography";

const MainInstructions = () => {
  return (
    <Typography sx={{ p: 1 }}>
      1. Add nodes
      <br /> 2. Edit nodes
      <br />
      3. Once you are finished with tasks press "Generate Playbook". Your
      playbook will be displayed in text editor window{" "}
    </Typography>
  );
};

export default MainInstructions;
