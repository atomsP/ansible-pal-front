import { useState, useContext, useEffect } from "react";
import { ProSidebar, Menu, MenuItem } from "react-pro-sidebar";
import { Box, IconButton, Typography, useTheme } from "@mui/material";
import { Link } from "react-router-dom";

import "react-pro-sidebar/dist/css/styles.css";
import { ColorModeContext } from "../../theme";

import MenuOutlinedIcon from "@mui/icons-material/MenuOutlined";
import BorderColorOutlinedIcon from "@mui/icons-material/BorderColorOutlined";
import InventoryOutlinedIcon from "@mui/icons-material/InventoryOutlined";
import MenuBookOutlinedIcon from "@mui/icons-material/MenuBookOutlined";
import HubOutlinedIcon from "@mui/icons-material/HubOutlined";
import AutofpsSelectOutlinedIcon from "@mui/icons-material/AutofpsSelectOutlined";
import LiveHelpOutlinedIcon from "@mui/icons-material/LiveHelpOutlined";
import LightModeOutlinedIcon from "@mui/icons-material/LightModeOutlined";
import DarkModeOutlinedIcon from "@mui/icons-material/DarkModeOutlined";
import GrainIcon from "@mui/icons-material/Grain";
import QuestionMarkIcon from "@mui/icons-material/QuestionMark";

const Item = ({
  title,
  to,
  icon,
  selected,
  setSelected,
  disabled,
  collapsed,
}) => {
  const [isHovered, setIsHovered] = useState(false);
  const theme = useTheme();

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  return (
    <MenuItem
      active={selected === title}
      style={{
        color: disabled
          ? theme.palette.sidebar.disabled
          : theme.palette.sidebar.menuEntry,
      }}
      icon={icon}
      onClick={() => {
        if (!disabled) {
          setSelected(title);
        }
      }}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <Typography>{title}</Typography>
      {isHovered && collapsed && (
        <Box
          sx={{
            position: "absolute",
            top: 0,
            right: 0,
            width: "100%",
            height: "100%",
            backgroundColor: "rgba(0, 0, 0, 0.5)",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            zIndex: 1,
          }}
        >
          <Typography variant="caption" color="inherit">
            <Link to={to}>{title}</Link>
          </Typography>
        </Box>
      )}
      {!disabled && <Link to={to} />}
      {disabled && (
        <span
          style={{
            marginLeft: "10px",
            color: theme.palette.sidebar.announcement,
          }}
        >
          Coming soon
        </span>
      )}
    </MenuItem>
  );
};

const Sidebar = () => {
  const theme = useTheme();
  const colorMode = useContext(ColorModeContext);
  const [isCollapsed, setIsCollapsed] = useState(false);
  const [selected, setSelected] = useState("Home");

  const handleResize = () => {
    const threshold = 1200;
    setIsCollapsed(window.innerWidth <= threshold);
  };

  useEffect(() => {
    const initialWindowWidth = window.innerWidth;
    setIsCollapsed(initialWindowWidth <= initialWindowWidth / 2);

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <Box
      sx={{
        "& .pro-sidebar-inner": {
          background: theme.palette.sidebar.background,
        },
        "& .pro-icon-wrapper": {
          backgroundColor: "transparent !important",
        },
        "& .pro-inner-item": {
          padding: "5px 35px 5px 20px !important",
        },

        "& .pro-inner-item:hover": {
          color: theme.palette.sidebar.hover,
        },
        "& .pro-menu-item.active": {
          color: theme.palette.sidebar.active,
        },
      }}
    >
      <ProSidebar collapsed={isCollapsed}>
        <Menu iconShape="square">
          <MenuItem
            onClick={() => setIsCollapsed(!isCollapsed)}
            icon={isCollapsed && <MenuOutlinedIcon />}
            style={{
              margin: "10px 0 20px 0",
              color: theme.palette.sidebar.collapsedIcon,
            }}
          >
            {!isCollapsed && (
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                ml="15px"
              >
                <Typography variant="h3" color={theme.palette.sidebar.logo}>
                  ansiblePal
                </Typography>
                <IconButton onClick={() => setIsCollapsed(!isCollapsed)}>
                  <MenuOutlinedIcon />
                </IconButton>
              </Box>
            )}
          </MenuItem>

          <Box paddingLeft={isCollapsed ? undefined : "10%"}>
            <Item
              title="Home"
              to="/"
              icon={<AutofpsSelectOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
              collapsed={isCollapsed}
            />
            <Item
              title="Tutorial"
              to="/tutorial"
              icon={<QuestionMarkIcon />}
              selected={selected}
              setSelected={setSelected}
              collapsed={isCollapsed}
            />

            <Item
              title="YAML Validator"
              to="/validator"
              icon={<BorderColorOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
              collapsed={isCollapsed}
            />

            <Typography
              variant="h6"
              color={theme.palette.sidebar.menuSplit}
              sx={{ m: "15px 0 5px 20px" }}
            >
              {isCollapsed ? "Create" : "Playbook Builder"}
            </Typography>

            <Item
              title="Inventory Builder"
              to="/inventory"
              icon={<InventoryOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
              collapsed={isCollapsed}
            />

            <Item
              title="Playbook Builder"
              to="/playbook"
              icon={<MenuBookOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
              collapsed={isCollapsed}
            />
            <Item
              title="Playbook analyzer"
              to="/playbook_analyzer"
              icon={<GrainIcon />}
              selected={selected}
              setSelected={setSelected}
              collapsed={isCollapsed}
            />

            <Item
              disabled="true"
              title="Network Nodes"
              to="/network"
              icon={<HubOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
              collapsed={isCollapsed}
            />
            <Typography
              variant="h6"
              color={theme.palette.sidebar.menuSplit}
              sx={{ m: "15px 0 5px 20px" }}
            >
              {isCollapsed ? "Help" : "Help Section"}
            </Typography>
            {
              /* <Typography
              variant="h6"
              color={colors.grey[300]}
              sx={{ m: "15px 0 5px 20px" }}
            >
              User Account
            </Typography>

            <Item
              title="Login"
              to="/login"
              icon={<PersonOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
            />

            <Item
              title="Sign Up"
              to="/signup"
              icon={<AddCircleOutlineOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
            />

            <Typography
              variant="h6"
              color={colors.grey[300]}
              sx={{ m: "15px 0 5px 20px" }}
            >
              Settings
            </Typography> */

              <MenuItem
                onClick={colorMode.toggleColorMode}
                icon={
                  theme.palette.mode === "dark" ? (
                    <LightModeOutlinedIcon
                      style={{ color: theme.palette.sidebar.modeSwitchIcon }}
                    />
                  ) : (
                    <DarkModeOutlinedIcon
                      style={{ color: theme.palette.sidebar.modeSwitchIcon }}
                    />
                  )
                }
                selected={selected}
              >
                <Typography
                  sx={{ color: theme.palette.sidebar.modeSwitchText }}
                >
                  {theme.palette.mode === "dark" ? "Light Mode" : "Dark Mode"}
                </Typography>
              </MenuItem>
            }

            <Item
              title="FAQ"
              to="/FAQ"
              icon={<LiveHelpOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
              collapsed={isCollapsed}
            />
          </Box>
        </Menu>
      </ProSidebar>
    </Box>
  );
};

export default Sidebar;
