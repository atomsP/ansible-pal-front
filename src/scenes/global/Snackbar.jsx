import React, { useState, useEffect } from "react";
import Snackbar from "@material-ui/core/Snackbar";
import Slide from "@mui/material/Slide";
import { makeStyles } from "@material-ui/core/styles";
import { useTheme } from "@mui/material";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";

function SlideTransition(props) {
  return <Slide {...props} direction="up" />;
}

const MessageSnackbar = ({ open, message, onClose, messageType }) => {
  const theme = useTheme();
  const [isHovered, setIsHovered] = useState(false);

  const useStyles = makeStyles(() => ({
    snackbar: {
      fontFamily: "Courier New",
      letterSpacing: "1px",
      backgroundColor:
        messageType === "error"
          ? theme.palette.primary.errorMessageBackground
          : messageType === "warning"
          ? theme.palette.primary.warningMessageBackground
          : messageType === "info"
          ? theme.palette.primary.infoMessageBackground
          : undefined,
      color:
        messageType === "error"
          ? theme.palette.primary.errorMessageText
          : messageType === "warning"
          ? theme.palette.primary.warningMessageText
          : messageType === "info"
          ? theme.palette.primary.infoMessageText
          : undefined,
      borderRadius: 0,
    },
    closeIcon: {
      color: isHovered
        ? theme.palette.primary.errorMessageText
        : theme.palette.primary.warningMessageText,
    },
  }));

  const classes = useStyles();
  const handleClose = (event, reason) => {
    // if (reason === "clickaway") {
    //   return;
    // }
    onClose();
  };

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  return (
    <Snackbar
      open={Boolean(open)}
      onClose={handleClose}
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      message={"[" + message + "]"}
      TransitionComponent={SlideTransition}
      ContentProps={{
        className: classes.snackbar,
      }}
      action={
        <IconButton
          size="small"
          aria-label="close"
          color="inherit"
          onClick={handleClose}
          onMouseEnter={handleMouseEnter}
          onMouseLeave={handleMouseLeave}
        >
          <CloseIcon fontSize="small" className={classes.closeIcon} />
        </IconButton>
      }
    />
  );
};

export default MessageSnackbar;
