import React from "react";
import PacmanLoader from "react-spinners/PacmanLoader";
import { useTheme } from "@mui/material";

const LoadingAnimation = () => {
  const theme = useTheme();
  return (
    <PacmanLoader
      color={theme.palette.primary.animation}
      size={30}
      aria-label="Loading Spinner"
      data-testid="loader"
    />
  );
};

export default LoadingAnimation;
