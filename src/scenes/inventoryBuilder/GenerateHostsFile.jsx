import React, { useState, useContext } from "react";
import { useMediaQuery } from "react-responsive";

import { AxiosContext } from "../../services/AxiosContext";
import { Box, useTheme } from "@mui/material";

import AceEditor from "react-ace";
import "ace-builds/src-noconflict/mode-plain_text";
import "ace-builds/src-noconflict/theme-gob";
import "ace-builds/src-noconflict/theme-github";

import "ace-builds/src-noconflict/mode-javascript";
import img from "../../assets/ansibleInventory.png";
import "../global/ace.css";
import { GenerateButton } from "../../components/components";
import MessageSnackbar from "../global/Snackbar";
import {
  InputTextfield,
  ConfirmationDialog,
  SimpleButton,
} from "../../components/components";

const Inventory = () => {
  const axiosContext = useContext(AxiosContext);
  const [groups, setGroups] = useState([]);
  const [message, setMessage] = useState(null);
  const [messageType, setMessagetype] = useState(null);

  const [newGroupName, setNewGroupName] = useState("");
  const [newHostsList, setNewHostsList] = useState("");
  const [hostsFileContent, setHostsFileContent] = useState("");
  const [fileGenerated, setFileGenerated] = useState(false);
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const [groupFocused, setGroupFocused] = useState(false);
  const [hostsFocused, setHostsFocused] = useState(false);
  const [isConfirmationOpen, setIsConfirmationOpen] = useState(false);

  const theme = useTheme();

  const isDesktop = useMediaQuery({ minWidth: 1128 });
  const isSmall = useMediaQuery({ maxWidth: 610 });

  const handleGroupFocus = () => {
    setGroupFocused(true);
  };

  const handleGroupBlur = () => {
    setGroupFocused(false);
  };
  const handleHostsFocus = () => {
    setHostsFocused(true);
  };

  const handleHostsBlur = () => {
    setHostsFocused(false);
  };

  const handleNewGroupNameChange = (event) => {
    const groupName = event.target.value;
    setNewGroupName(groupName.trim() || "ungrouped");
  };

  const handleNewHostsListChange = (event) => {
    setNewHostsList(event.target.value);
  };

  const handleAddGroup = () => {
    const trimmedGroupName = newGroupName.trim();
    const trimmedHostsList = newHostsList.trim();

    if (trimmedHostsList !== "") {
      let newGroup;
      if (trimmedGroupName === "") {
        newGroup = groups.find((group) => !group.name);
        if (!newGroup) {
          newGroup = { name: "", hosts: [] };
          setGroups([...groups, newGroup]);
        }
      } else {
        newGroup = groups.find((group) => group.name === trimmedGroupName);
        if (!newGroup) {
          newGroup = { name: trimmedGroupName, hosts: [] };
          setGroups([...groups, newGroup]);
        }
      }
      newGroup.hosts.push(
        ...trimmedHostsList.split("\n").filter((line) => line.trim() !== "")
      );
      setNewHostsList("");
      setFileGenerated(false);
    } else {
      const errorMessage = `Add group name and hosts`;
      setOpen(true);
      setMessage(errorMessage);
      setMessagetype("warning");
    }
    setNewGroupName("");
  };

  const handleKeyPress = (event) => {
    if (event.key === " ") {
      event.preventDefault();
    }
  };

  const handleGenerateHostsFile = () => {
    if (groups.length > 0) {
      setLoading(true);
      const data = {};
      groups.forEach((group) => {
        data[group.name] = group.hosts;
      });
      axiosContext.publicAxios
        .post(
          `ansible_inventory_backwards/`,

          {
            data: data,
          },
          { timeout: 3500 }
        )
        .then((response) => {
          setHostsFileContent(response.data);
          setFileGenerated(true);
          setLoading(false);
          const errorMessage = "Download is ready";
          setMessage(errorMessage);
          setMessagetype("info");
          setOpen(true);
        })
        .catch((error) => {
          if (error.response) {
            const errorMessage = `Server responded with an error: ${error.response.data}`;
            setMessage(errorMessage);
            setMessagetype("error");
            setOpen(true);
            setLoading(false);
          } else if (error.request) {
            const errorMessage = "No response received from the server";
            setMessage(errorMessage);
            setOpen(true);
            setLoading(false);
            setMessagetype("error");
          } else {
            const errorMessage = `Error occurred while sending the request: ${error.message}`;
            setMessage(errorMessage);
            setOpen(true);
            setMessagetype("error");
            setLoading(false);
          }
        });
    } else {
      const errorMessage = "Please add at least one group";
      setMessage(errorMessage);
      setOpen(true);
      setMessagetype("warning");
      setLoading(false);
    }
  };

  const downloadHostsFile = () => {
    const content = hostsFileContent.content.replace(/\\n/g, "\n").trim();
    const file = new Blob([content], { type: "text/plain; charset=utf-8" });
    const fileURL = URL.createObjectURL(file);
    const link = document.createElement("a");
    link.href = fileURL;
    link.setAttribute("download", "hosts.txt");
    document.body.appendChild(link);
    link.click();
  };

  const handleDeleteAll = () => {
    setIsConfirmationOpen(true);
  };
  const handleConfirmClear = (key) => {
    setIsConfirmationOpen(false);
    setGroups([]);
    setFileGenerated(false);
  };
  const handleCloseConfirmation = () => {
    setIsConfirmationOpen(false);
  };

  const imgHeight = 250;

  function formatForAceEditor(obj) {
    let output = "";

    obj.forEach((group) => {
      const { name, hosts } = group;
      const groupName = name.trim() === "" ? "" : `[${name.trim()}]`;

      if (groupName !== "") {
        output += `${groupName}\n`;
      }

      hosts.forEach((ip) => {
        output += `${ip}\n`;
      });
      output += "\n";
    });

    return output;
  }

  const handleSnackbarClose = () => {
    setOpen(false);
  };

  const result = formatForAceEditor(groups);

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: !isDesktop ? "column" : "row",
        alignItems: !isDesktop ? "center" : undefined,
        justifyContent: !isDesktop ? "center" : undefined,
        width: "100%",
        height: "100%",
      }}
    >
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          width: isSmall ? "50%" : "100%",
        }}
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            height: "100%",
          }}
        >
          <InputTextfield
            id="group-name-input"
            focused={groupFocused}
            label={"Group name"}
            rows={1}
            onChange={handleNewGroupNameChange}
            value={newGroupName}
            onFocus={handleGroupFocus}
            onBlur={handleGroupBlur}
            theme={theme}
          />
          <InputTextfield
            id="hosts-list-input"
            focused={hostsFocused}
            label={"Hosts list"}
            rows={13}
            onChange={handleNewHostsListChange}
            value={newHostsList}
            onFocus={handleHostsFocus}
            onBlur={handleHostsBlur}
            theme={theme}
            onKeyPress={handleKeyPress}
          />
        </Box>
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            paddingTop: !isDesktop ? "20px" : undefined,
            paddingBottom: !isDesktop ? "10px" : undefined,
          }}
        >
          <SimpleButton onClick={handleAddGroup} text="[Add]" theme={theme} />
          <GenerateButton
            onClick={handleGenerateHostsFile}
            disabled={loading}
            buttonText="[Generate]"
            loading={loading}
          />
          {groups.length !== 0 && fileGenerated && (
            <SimpleButton
              onClick={handleDeleteAll}
              text="[Clear]"
              theme={theme}
            />
          )}
          <ConfirmationDialog
            theme={theme}
            open={isConfirmationOpen}
            onClose={handleCloseConfirmation}
            title="Clear hosts?"
            content="All contents and download data will be removed"
            onClickNo={handleCloseConfirmation}
            onClickYes={() => handleConfirmClear()}
          />
          {fileGenerated && groups.length !== 0 && (
            <SimpleButton
              onClick={downloadHostsFile}
              text="[Download]"
              theme={theme}
            />
          )}
        </Box>
      </Box>
      <Box>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: "30rem",
            width: "100vh",
          }}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            {groups.length !== 0 ? (
              <AceEditor
                mode="plain_text"
                theme={theme.palette.mode === "dark" ? "gob" : "github"}
                value={result}
                editorProps={{ $blockScrolling: Infinity }}
                style={{
                  right: !isSmall ? "30px" : undefined,
                  width: isSmall ? "25rem" : "30rem",
                }}
                className={
                  theme.palette.mode === "dark"
                    ? "ace-dark-theme"
                    : "ace-light-theme"
                }
              />
            ) : (
              <Box>
                <img
                  alt="ansible-flag"
                  style={{ height: imgHeight, width: imgHeight * 1.62 }}
                  src={img}
                />
              </Box>
            )}
          </Box>
        </Box>
      </Box>
      <MessageSnackbar
        open={open}
        message={message}
        onClose={handleSnackbarClose}
        messageType={messageType}
      />
    </Box>
  );
};

export default Inventory;
