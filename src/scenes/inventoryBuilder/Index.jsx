import React, { useState } from "react";
import { useMediaQuery } from "react-responsive";

import GenerateHostsFile from "./GenerateHostsFile";
import ParseHostsFile from "./ParseHostsFile";
import Button from "@mui/material/Button";
import Header from "../../components/Header";
import { Box } from "@mui/material";
import { useTheme } from "@mui/material";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import InventoryBuilderInstructions from "../global/instructions/InventoryBuilderInstructions";

import {
  MainBox,
  HeadingBox,
  HeaderAndPopoverBox,
  ImportedComponentBox,
  PopoverTypography,
  PopoverComponent,
} from "../../components/components";

function ParserOrGenerator() {
  const [showComponentOne, setShowComponentOne] = useState(true);
  const [anchorEl, setAnchorEl] = React.useState(null);

  const theme = useTheme();

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  const toggleComponent = () => {
    setShowComponentOne(!showComponentOne);
  };

  const isDesktop = useMediaQuery({ minWidth: 1128 });

  const isSmall = useMediaQuery({ maxWidth: 610 });

  return (
    <MainBox>
      <HeadingBox>
        <Box
          p={2}
          sx={{
            display: "flex",
            flexDirection: "row",
          }}
        >
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              width: !isDesktop ? "15rem" : "16rem",
            }}
          >
            <HeaderAndPopoverBox>
              <Header subtitle="Your inventory" />
              <PopoverTypography
                open={open}
                handlePopoverOpen={handlePopoverOpen}
                handlePopoverClose={handlePopoverClose}
                icon={<InfoOutlinedIcon />}
                theme={theme}
              />
              <PopoverComponent
                open={open}
                anchorEl={anchorEl}
                handlePopoverClose={handlePopoverClose}
                text={<InventoryBuilderInstructions />}
              />
            </HeaderAndPopoverBox>
            <Box>
              <input
                type="file"
                accept=".txt, .yml, .yaml"
                style={showComponentOne ? { visibility: "hidden" } : {}}
              />
            </Box>
          </Box>
          <Box>
            <Button
              onClick={toggleComponent}
              variant="contained"
              color="info"
              sx={{
                marginTop: "10px",
                borderRadius: 0,
                fontFamily: "Courier New",
                letterSpacing: "1px",
              }}
            >
              {isSmall
                ? "[Switch]"
                : showComponentOne
                ? "[Switch] to Visualizer"
                : "[Switch] to Builder"}
            </Button>
          </Box>
        </Box>
      </HeadingBox>
      <ImportedComponentBox>
        {showComponentOne ? <GenerateHostsFile /> : <ParseHostsFile />}
      </ImportedComponentBox>
    </MainBox>
  );
}

export default ParserOrGenerator;
