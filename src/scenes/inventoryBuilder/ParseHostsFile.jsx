import React, { useState, useContext } from "react";
import { AxiosContext } from "../../services/AxiosContext";
import { Box, useTheme } from "@mui/material";
import { useMediaQuery } from "react-responsive";

import { InputTextfield, SimpleButton } from "../../components/components";

const ParsedHostsFile = () => {
  const [hostsFile, setHostsFile] = useState("");
  const [setGenerated] = useState([]);
  const [inputFocused, setInputFocused] = useState(false);

  const axiosContext = useContext(AxiosContext);
  const theme = useTheme();

  const isDesktop = useMediaQuery({ minWidth: 1128 });
  const isSmall = useMediaQuery({ maxWidth: 610 });

  const handleHostsFileChange = (event) => {
    setHostsFile(event.target.value);
  };

  const handleInputFocus = () => {
    setInputFocused(true);
  };

  const handleInputBlur = () => {
    setInputFocused(false);
  };

  const ParseHostsFile = () => {
    axiosContext.publicAxios
      .post(`ansible_inventory/`, {
        content: hostsFile,
      })
      .then((response) => {
        setGenerated(response.data.data);
      })
      .catch((error) => {
        alert("Blunder..", "Hosts file could not be parsed.");
      });
  };

  // const GroupOutput = ({ data }) => {
  //   return (
  //     <div style={{ textAlign: "center" }}>
  //       <ul
  //         style={{
  //           listStyleType: "none",
  //           textAlign: "center",
  //           display: "inline",
  //         }}
  //       >
  //         {Object.keys(data).map((key) => (
  //           <li
  //             key={key}
  //             style={{ listStyleType: "none", textAlign: "center" }}
  //           >
  //             <h3>{key}</h3>
  //             <ul
  //               style={{
  //                 listStyleType: "none",
  //                 textAlign: "center",
  //                 display: "inline",
  //               }}
  //             >
  //               {data[key].map((item) => (
  //                 <li key={item}>{item}</li>
  //               ))}
  //             </ul>
  //           </li>
  //         ))}
  //       </ul>
  //     </div>
  //   );
  // };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: !isDesktop ? "column" : "row",
        alignItems: !isDesktop ? "center" : undefined,
        justifyContent: !isDesktop ? "center" : undefined,
        width: "100%",
        height: "100%",
      }}
    >
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          width: isSmall ? "50%" : "100%",
        }}
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            height: "100%",
          }}
        >
          <InputTextfield
            id="hosts-file-input"
            focused={inputFocused}
            label="Inventory data"
            rows={15}
            value={hostsFile}
            onChange={handleHostsFileChange}
            onFocus={handleInputFocus}
            onBlur={handleInputBlur}
            theme={theme}
          />
        </Box>
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            paddingTop: !isDesktop ? "20px" : undefined,
            paddingBottom: !isDesktop ? "10px" : undefined,
          }}
        >
          <SimpleButton onClick={ParseHostsFile} text="[SHOW]" theme={theme} />
        </Box>
      </Box>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          height: "30rem",
          width: "100vh",
        }}
      ></Box>
    </Box>
  );
};

export default ParsedHostsFile;
