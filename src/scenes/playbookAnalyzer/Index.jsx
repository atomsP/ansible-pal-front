import React, { useState, useContext } from "react";
import Button from "@mui/material/Button";
import { useMediaQuery } from "react-responsive";

import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";

import { AxiosContext } from "../../services/AxiosContext";
import { Box, useTheme } from "@mui/material";
import TaskList from "./Analyzer";
import Header from "../../components/Header";
import MessageSnackbar from "../global/Snackbar";

import {
  MainBox,
  InputTextfield,
  ImportedComponentBox,
  HeadingBox,
  HeaderAndPopoverBox,
  PopoverTypography,
  PopoverComponent,
} from "../../components/components";

const Analyzer = () => {
  const [hostsFile, setHostsFile] = useState("");
  const [generated, setGenerated] = useState([]);
  const [inputFocused, setInputFocused] = useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [message, setMessage] = useState(null);
  const [messageType, setMessagetype] = useState(null);

  const axiosContext = useContext(AxiosContext);
  const theme = useTheme();

  const isDesktop = useMediaQuery({ minWidth: 1128 });
  const isSmall = useMediaQuery({ maxWidth: 610 });

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const handleSnackbarClose = () => {
    setMessage(null);
  };

  const open = Boolean(anchorEl);

  const handleHostsFileChange = (event) => {
    setHostsFile(event.target.value);
  };

  const handleInputFocus = () => {
    setInputFocused(true);
  };

  const handleInputBlur = () => {
    setInputFocused(false);
  };

  const ParseHostsFile = () => {
    if (!hostsFile.trim()) {
      alert("No playbook provided");
      return;
    }

    axiosContext.publicAxios
      .post(
        `parse_ansible_playbook/`,
        { timeout: 3500 },
        {
          content: hostsFile,
        }
      )
      .then((response) => {
        setGenerated(response.data.data);
      })
      .catch((error) => {
        if (error.response) {
          const errorMessage = `Server responded with an error`;
          setMessage(errorMessage);
          setMessagetype("error");
        } else if (error.request) {
          const errorMessage = "No response received from the server";
          setMessage(errorMessage);
          setMessagetype("error");
        } else {
          const errorMessage = `Error occurred while sending the request`;
          setMessage(errorMessage);
          setMessagetype("error");
        }
      });
  };

  return (
    <MainBox>
      <HeadingBox>
        <Box
          p={2}
          sx={{
            display: "flex",
            flexDirection: "row",
          }}
        >
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              width: !isDesktop ? "15rem" : "16rem",
            }}
          >
            <HeaderAndPopoverBox>
              <Header subtitle="Playbook probe" />
              <PopoverTypography
                open={open}
                handlePopoverOpen={handlePopoverOpen}
                handlePopoverClose={handlePopoverClose}
                icon={<InfoOutlinedIcon />}
                theme={theme}
              />
              <PopoverComponent
                open={open}
                anchorEl={anchorEl}
                handlePopoverClose={handlePopoverClose}
                text="PLACEHOLEDER"
              />
            </HeaderAndPopoverBox>
          </Box>
        </Box>
      </HeadingBox>
      <ImportedComponentBox>
        <Box
          sx={{
            display: "flex",
            flexDirection: !isDesktop ? "column" : "row",
            alignItems: !isDesktop ? "center" : undefined,
            justifyContent: !isDesktop ? "center" : undefined,
            width: "100%",
            height: "100%",
          }}
        >
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              width: isSmall ? "50%" : "100%",
            }}
          >
            <Box
              className="home"
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
                height: "100%",
                overflowY: "hidden",
              }}
            >
              <InputTextfield
                id="hosts-file-input"
                focused={inputFocused}
                label={"Input ansible playbook"}
                rows={15}
                onChange={handleHostsFileChange}
                value={hostsFile}
                onFocus={handleInputFocus}
                onBlur={handleInputBlur}
                theme={theme}
              />
              <Button
                style={{
                  margin: "1.5em",
                  width: "10rem",
                  height: "3em",
                  borderRadius: 0,
                  fontFamily: "Courier New",
                  letterSpacing: "1px",
                }}
                variant="contained"
                color="info"
                size="large"
                onClick={ParseHostsFile}
              >
                [DISSECT]
              </Button>
            </Box>
          </Box>
          <Box className="away">
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                height: "23rem",
                width: "100vh",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <div>
                  <TaskList data={generated} />
                </div>
              </Box>
            </Box>
          </Box>
        </Box>
        <MessageSnackbar
          open={message}
          message={message}
          onClose={handleSnackbarClose}
          messageType={messageType}
        />
      </ImportedComponentBox>
    </MainBox>
  );
};

export default Analyzer;
