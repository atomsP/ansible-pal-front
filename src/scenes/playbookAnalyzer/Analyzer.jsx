import React from "react";
import { Box } from "@mui/system";
import { useTheme } from "@mui/material";

import { useMediaQuery } from "react-responsive";

function TaskList({ data }) {
  const isDesktop = useMediaQuery({ minWidth: 1128 });
  const isSmall = useMediaQuery({ maxWidth: 610 });
  const theme = useTheme();
  return (
    <Box
      sx={{
        paddingLeft: "2rem",
        width: isSmall ? "20rem" : "30rem",
        paddingTop: isDesktop ? "25rem" : "40rem",
      }}
    >
      {data.map((task, index) => (
        <div key={index}>
          <h3>Task {task["Task number"]}</h3>
          <h3>{task["Task name"]}</h3>
          <p>
            <strong>Collection:</strong> {task["Collection title"]}
          </p>
          <p>
            <strong>Collection URL:</strong>{" "}
            <a
              href={task["Collection URL"]}
              style={{ color: theme.palette.mode === "dark" && "yellow" }}
            >
              {task["Collection URL"]}
            </a>
          </p>
          <p>
            <strong>Module:</strong> {task["Module title"]}
          </p>
          <p>
            <strong>Module Description:</strong> {task["Module description"]}
          </p>
          <p>
            <strong>Module URL:</strong>{" "}
            <a
              href={task["Module URL"]}
              style={{ color: theme.palette.mode === "dark" && "yellow" }}
            >
              {task["Module URL"]} style=
            </a>
          </p>
          <h4>Parameters:</h4>
          <ul>
            {task["Parameters"].map((parameter, index) => (
              <li key={index}>
                <p>
                  <strong>{parameter["Parameter name"]}:</strong>{" "}
                  {parameter["Parameter value"]}
                </p>
                <p>
                  <strong>Data type expected:</strong>{" "}
                  {parameter["Data type expected"]}
                </p>
                <p>
                  <strong>Parameter description:</strong>{" "}
                  {parameter["Comments about parameter"]}
                </p>
              </li>
            ))}
          </ul>
        </div>
      ))}
    </Box>
  );
}

export default TaskList;
