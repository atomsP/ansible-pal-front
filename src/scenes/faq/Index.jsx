import { Box, useTheme } from "@mui/material";
import Header from "../../components/Header";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

const faqData = [
  {
    question: "How to use YAML validator?",
    answer:
      "Just upload yaml file into it and look for warning flags. Sometimes\
    error indicates mistake in a row before or after the actual error.",
  },
  {
    question: "How to use Playbook Builder?",
    answer: "Add nodes to Play, add attributes to nodes, create it.",
  },
  {
    question: "Is Ansible data up-to-data?",
    answer: "Sure, data is just scraped from Ansible documentation",
  },
  {
    question: "How soon is Network Nodes visualizer coming?",
    answer: "Relatively soon",
  },
  {
    question: "How can I contact you?",
    answer: "Via e-mail notinproductionyet@ansiblepal.com",
  },
];

const FAQ = () => {
  const theme = useTheme();

  return (
    <Box m="20px" fontFamily="Courier New">
      <Header title="[FAQ]" subtitle="[Frequently Asked Questions Page]" />

      {faqData.map((item, index) => (
        <Accordion
          key={index}
          defaultExpanded
          backgroundcolor={theme.palette.accordion.background}
        >
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography color={theme.palette.accordion.typography} variant="h5">
              {item.question}
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>{item.answer}</Typography>
          </AccordionDetails>
        </Accordion>
      ))}
    </Box>
  );
};

export default FAQ;
