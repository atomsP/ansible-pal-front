import React, { useEffect, useState, useRef, useContext } from "react";
import { IntroContext } from "../../services/IntroContext";
import { useTheme } from "@emotion/react";
import "./typewriterText.css";

const TypewriterText = ({ text }) => {
  const { setVisited } = useContext(IntroContext);
  const [displayText, setDisplayText] = useState("");
  const [currentIndex, setCurrentIndex] = useState(0);
  const [isTyping, setIsTyping] = useState(true);
  const containerRef = useRef(null);

  const theme = useTheme();

  useEffect(() => {
    let interval;
    const intervalFunction = () => {
      if (currentIndex === text.length) {
        setIsTyping(false);
        clearInterval(interval);
        setTimeout(() => {
          setVisited(true);
        }, 8000);
      } else {
        const currentChar = text[currentIndex];
        if (currentChar === "." || currentChar === "!" || currentChar === "?") {
          setIsTyping(false);
          setDisplayText((prevText) => prevText + currentChar);
          clearInterval(interval);
          setTimeout(() => {
            setCurrentIndex((prevIndex) => prevIndex + 1);
            scrollPage();
            interval = setInterval(
              intervalFunction,
              Math.floor(Math.random() * 200) + 500
            );
            setIsTyping(true);
          }, Math.floor(Math.random() * 200) + 500);
        } else {
          if (currentChar === "\n") {
            setDisplayText((prevText) => prevText + "<br />");
          } else {
            setDisplayText((prevText) => prevText + currentChar);
          }
          setCurrentIndex((prevIndex) => prevIndex + 1);
          scrollPage();
        }
      }
    };
    interval = setInterval(
      intervalFunction,
      Math.floor(Math.random() * 130) + 40
    );
    return () => clearInterval(interval);
  }, [currentIndex, text]);

  const scrollPage = () => {
    if (containerRef.current) {
      const container = containerRef.current;
      container.scrollBy({
        top: container.scrollHeight,
        behavior: "smooth",
      });
    }
  };

  return (
    <div ref={containerRef}>
      <div
        className={!isTyping ? "typewriter-text" : "typewriter-stop"}
        style={{
          "--caret-background-color": theme.palette.primary.typewriterCaret,
        }}
        dangerouslySetInnerHTML={{ __html: displayText }}
      />
    </div>
  );
};

export default TypewriterText;
