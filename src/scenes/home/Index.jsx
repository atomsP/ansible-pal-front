import { Box } from "@mui/material";
import { useMediaQuery } from "react-responsive";

import Header from "../../components/Header";
import TypewriterText from "./TypewriterText";
import { Button } from "@mui/material";
import { useContext } from "react";
import { IntroContext } from "../../services/IntroContext";
import Functions from "./Functions";

const Home = () => {
  const { visited, setVisited } = useContext(IntroContext);

  const isMobile = useMediaQuery({ maxWidth: 767 });

  const text =
    "Welcome, esteemed denizens of the digital realm, to the hallowed halls of the Online YAML Syntax Checker, Ansible Playbook, and Hosts File Generator! Gather round, ye intrepid adventurers of the scripting universe, for a journey through the realms of automation and orchestration awaits." +
    "\n\nBehold, for this sanctified tool shall bring forth the power to tame the unruly YAML syntax, with its meticulous gaze and discerning eye. No more shall you fret over missing colons or misplaced indentation, for this elegant sentinel shall ensure your YAML files bask in the glory of structural harmony." +
    "\n\nBut lo, the splendor does not cease with the syntax alone! Marvel at the grace of the Ansible playbook generator, a veritable maestro of orchestration. With a mere flick of thy keyboard, it shall compose symphonies of tasks and configurations, seamlessly choreographing your infrastructure's every move. From provisioning servers to orchestrating deployments, this virtuoso shall grant you the power to orchestrate with unparalleled finesse." +
    "\n\nAnd what of the hosts file generator, you ask? Fear not, dear seeker of network enlightenment, for this humble yet indispensable tool shall bestow upon you the means to shape your network's destiny. Whether it be grouping hosts by their ethereal roles or mapping IP addresses to celestial names, this generator shall be your guide through the realms of networking wonders." +
    "\n\nSo, fair travelers, step forth and immerse yourselves in this fusion of pragmatism and artistry, where YAML finds solace, playbooks become symphonies, and hosts are guided by the invisible hand of digital wizardry. May the code be ever in your favor as you explore the boundless possibilities that lie within these virtual corridors. Onward, to automation enlightenment!";

  return (
    <Box p="20px" sx={{ overflow: "auto" }}>
      {!visited ? (
        <>
          <Box
            display="flex"
            justifyContent="space-between"
            sx={{
              flexDirection: isMobile ? "column" : "row",
            }}
          >
            <Header title="WELCOME" />
            <Box>
              <Button
                sx={{ m: 1.5 }}
                color="info"
                size="small"
                onClick={() => {
                  setVisited(true);
                }}
              >
                SKIP
              </Button>
              <audio controls loop style={{ paddingBottom: "10px" }}>
                <source src="/radien.mp3" type="audio/mp3" />
                Your browser does not support the audio element.
              </audio>
            </Box>
          </Box>
          <TypewriterText text={text} />
        </>
      ) : (
        <Box m="20px">
          <Functions />
        </Box>
      )}
    </Box>
  );
};

export default Home;
