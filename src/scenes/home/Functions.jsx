import React, { useContext } from "react";
import Header from "../../components/Header";
import { Box } from "@mui/material";
import { Link, useLocation } from "react-router-dom";
import img from "../../assets/puzzle.png";
import { SelectedContext } from "../../services/SelectedContext";

const Functions = () => {
  const location = useLocation();
  const pathname = location.pathname;
  const { setSelected } = useContext(SelectedContext);
  return (
    <Box sx={{ display: "flex", flexDirection: "column", height: "80vh" }}>
      <Box className="first" sx={{ marginBottom: "auto" }}>
        <Header title="Ansible Pal"></Header>
        <Link to="/validator" style={{ textDecoration: "none" }}>
          <Header
            other="Validator for validating YAML syntax"
            active={pathname === "/validator"}
            onClick={() => setSelected("Validator")}
          ></Header>
        </Link>
        <Link to="/playbook" style={{ textDecoration: "none" }}>
          <Header
            other="Playbook Builder for generating Ansible playbooks"
            active={pathname === "/playbook"}
            onClick={() => setSelected("Playbook")}
          ></Header>
        </Link>
        <Link to="/inventory" style={{ textDecoration: "none" }}>
          <Header
            other="Inventory Builder for generating hosts files"
            active={pathname === "/inventory"}
            onClick={() => setSelected("Inventory")}
          ></Header>
        </Link>
      </Box>
      <Box className="second" sx={{ marginLeft: "auto" }}>
        <img
          src={img}
          alt="puzzleImage"
          style={{ width: "380px", height: "auto" }}
        />
      </Box>
    </Box>
  );
};

export default Functions;
