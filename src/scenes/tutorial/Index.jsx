import React, { useState, useContext, useEffect } from "react";
import { AxiosContext } from "../../services/AxiosContext";
import { Box, useTheme } from "@mui/material";
import LoadingAnimation from "../global/LoadingAnimation";

import MessageSnackbar from "../global/Snackbar";
import { PacmanBox, NoConnectionBox } from "../../components/components";
import img from "../../assets/noConnection.png";

const Tutorial = () => {
  const [loading, setLoading] = useState(false);
  const [generated, setGenerated] = useState([]);
  const [message, setMessage] = useState(null);
  const [messageType, setMessagetype] = useState(null);
  const [imgLoaded, setImgLoaded] = useState(false);

  const axiosContext = useContext(AxiosContext);
  const theme = useTheme();

  const parseHostsFile = () => {
    setLoading(true);
    axiosContext.publicAxios
      .get(`code-snippets/`, { timeout: 3500 })
      .then((response) => {
        setGenerated(response.data);
        setLoading(false);
      })
      .catch((error) => {
        if (error.response) {
          const errorMessage = `Server responded with an error: ${error.response.data}`;
          setMessage(errorMessage);
          setMessagetype("error");
          setLoading(false);
        } else if (error.request) {
          const errorMessage = "No response received from the server";
          setMessage(errorMessage);
          setMessagetype("error");
          setLoading(false);
        } else {
          const errorMessage = `Error occurred while sending the request: ${error.message}`;
          setMessage(errorMessage);
          setMessagetype("error");
          setLoading(false);
        }
      });
  };

  useEffect(() => {
    parseHostsFile();
  }, []);

  const handleSnackbarClose = () => {
    setMessage(null);
  };

  const imgHeight = 250;
  const Illustrate = () => {
    return (
      <img
        alt="no-connection"
        style={{
          height: imgHeight,
          width: imgHeight * 1.62,
          visibility: imgLoaded ? "visible" : "hidden",
        }}
        src={img}
        onLoad={() => setImgLoaded(true)}
      />
    );
  };

  return (
    <>
      {loading ? (
        <PacmanBox>
          <LoadingAnimation />
        </PacmanBox>
      ) : generated.length === 0 ? (
        <NoConnectionBox>
          {!imgLoaded && <div style={{ height: imgHeight }} />}
          <Illustrate />
          <MessageSnackbar
            open={message}
            message={message}
            onClose={handleSnackbarClose}
            messageType={messageType}
          />
        </NoConnectionBox>
      ) : (
        <Box display="flex" justifyContent="flex-start">
          <div style={{ maxWidth: "40rem" }}>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <h2 style={{ margin: "1em" }}>Script examples</h2>
            </div>
            {generated.map((snippet) => (
              <div
                key={snippet.id}
                style={{
                  margin: "1em",
                  border: "1px solid white",
                  padding: "1em",
                  backgroundColor: theme.palette.inputForm.background,
                }}
              >
                <h3>{snippet.title}</h3>
                <p>{snippet.description}</p>

                <pre style={{ fontSize: "12px" }}>{snippet.content}</pre>
              </div>
            ))}
          </div>
        </Box>
      )}
    </>
  );
};

export default Tutorial;
