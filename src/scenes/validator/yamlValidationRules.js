export const validateYaml = (yamlObject) => {
  const annotations = [];

  if (!yamlObject.host) {
    const existingErrorAnnotation = annotations.find(
      (annotation) =>
        annotation.type === "error" &&
        annotation.text === "Did you mean 'hosts'?"
    );

    if (!existingErrorAnnotation) {
      const errorAnnotation = {
        row: 0,
        column: 0,
        text: "Did you mean 'hosts'?",
        type: "error",
      };

      annotations.push(errorAnnotation);
    }
  } else {
    if (yamlObject.hosts) {
      const removeErrorAnnotation = {
        row: 0,
        column: 0,
        text: "",
        type: "",
      };

      annotations.push(removeErrorAnnotation);
    }
  }

  return annotations;
};
