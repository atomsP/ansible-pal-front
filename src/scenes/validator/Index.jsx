import React, { useState, useEffect, useRef } from "react";
import yaml from "js-yaml";
import { useMediaQuery } from "react-responsive";

import { useTheme } from "@mui/material";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import { Box } from "@mui/material";
import AceEditor from "react-ace";

import "ace-builds/src-noconflict/mode-plain_text";
import "ace-builds/src-noconflict/theme-gob";
import "ace-builds/src-noconflict/theme-github";

import Header from "../../components/Header";
import YamlValidatorInstructions from "../global/instructions/YamlValidatorInstructions";
import "../global/ace.css";

import {
  MainBoxNoFlex,
  MainHeaderBox,
  LeftHeaderBox,
  PopoverTypography,
  PopoverComponent,
  SimpleButton,
} from "../../components/components";

const Validator = () => {
  const [yamlData, setYamlData] = useState("");
  const [anchorEl, setAnchorEl] = useState(null);
  const editorRef = useRef(null);
  const theme = useTheme();

  const isMobile = useMediaQuery({ maxWidth: 767 });

  useEffect(() => {
    const content = localStorage.getItem("yamlData");
    if (content) setYamlData(content);
  }, []);

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const handleClearClick = () => {
    setYamlData("");
  };

  const open = Boolean(anchorEl);

  const handleFileChange = (event) => {
    const file = event.target.files[0];

    if (!file) return;

    const allowedExtensions = ["txt", "yml", "yaml"];
    const extension = file.name.split(".").pop();
    if (!allowedExtensions.includes(extension)) {
      alert("Invalid file. Only .txt, .yml, or .yaml files are allowed.");
      return;
    }

    const reader = new FileReader();
    reader.onload = (event) => {
      const content = event.target.result;
      const allowedTypes = [
        "text/plain",
        "application/x-yaml",
        "application/yaml",
        "text/yaml",
      ];
      const fileType = file.type;
      if (!allowedTypes.includes(fileType)) {
        alert("Invalid file. Only .txt, .yml, or .yaml files are allowed.");
        return;
      }

      setYamlData(content);
    };
    reader.readAsText(file);
  };

  useEffect(() => {
    const handleResize = () => {
      if (editorRef.current) {
        editorRef.current.editor.resize();
      }
    };

    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const handleYamlChange = (newValue) => {
    setYamlData(newValue);
    localStorage.setItem("yamlData", newValue);
    try {
      yaml.load(newValue);
      if (editorRef.current) {
        editorRef.current.editor.getSession().clearAnnotations();
      }
    } catch (e) {
      if (editorRef.current) {
        editorRef.current.editor.getSession().setAnnotations([
          {
            row: e.mark.line,
            column: e.mark.column,
            text: e.reason,
            type: "error",
          },
        ]);
      }
    }
  };

  return (
    <MainBoxNoFlex>
      <MainHeaderBox
        sx={{ display: "flex", flexDirection: isMobile ? "column" : "row" }}
      >
        <LeftHeaderBox
          sx={{ display: "flex", flexDirection: isMobile ? "row" : "column" }}
        >
          <Header subtitle="Validate your YAML" />
          <PopoverTypography
            open={open}
            handlePopoverOpen={handlePopoverOpen}
            handlePopoverClose={handlePopoverClose}
            icon={<InfoOutlinedIcon />}
            theme={theme}
          />
          <PopoverComponent
            open={open}
            anchorEl={anchorEl}
            handlePopoverClose={handlePopoverClose}
            text={<YamlValidatorInstructions />}
          />
        </LeftHeaderBox>
        <Box sx={{ display: "flex", flexDirection: "column" }}>
          <input
            type="file"
            accept=".txt, .yml, .yaml"
            onChange={handleFileChange}
          />
          {yamlData && (
            <SimpleButton
              onClick={handleClearClick}
              text="[Clear]"
              width="11rem"
              theme={theme}
              margin="5px 0px 5px 0px"
            />
          )}
        </Box>
      </MainHeaderBox>

      <AceEditor
        mode="yaml"
        theme={theme.palette.mode === "dark" ? "gob" : "github"}
        onChange={handleYamlChange}
        name="yaml-editor"
        editorProps={{ $blockScrolling: true }}
        height="30rem"
        width="100%"
        ref={editorRef}
        annotations={editorRef.current?.editor.getSession().getAnnotations()}
        value={yamlData}
        className={
          theme.palette.mode === "dark" ? "ace-dark-theme" : "ace-light-theme"
        }
      />
    </MainBoxNoFlex>
  );
};

export default Validator;
